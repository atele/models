from flask import current_app

from utilities import utils

config = utils.Payload(**current_app.config['ODOO_CONFIG'])
endpoints = utils.Payload(**current_app.config['ODOO_ENDPOINTS'])

# conn = odoo.RpcConnector(database=config.database, username=config.username, password=config.password,
#                          authenticate_endpoint=endpoints.authenticate, model_endpoint=endpoints.models)

