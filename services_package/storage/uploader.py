from messagerie import AWSEntity


aws_bucket_name = 'atele'

# AWSEntity.upload_file(file_path='/tmp/test.txt', bucket_name='atele', file_name='test.txt')
fp = "/home/kunsam002/projects/Atele/static/images/atele.png"
fn = "atele.png"


def upload_file(filename, filepath, container=None):
    if container and container not in ["", " "]:
        filename = "%s/%s" % (container, filename)
    url = AWSEntity.upload_file(file_path=filepath, bucket_name=aws_bucket_name, filename="%s" % filename)
    return url
