__author__ = 'stikks & kunsam002'

"""

This module is responsible for authenticating a user and handling permissions
for users in the system.

"""

from functools import wraps
from datetime import datetime, timedelta

from flask import redirect
from sqlalchemy import or_, and_
from flask_login import current_user
from flask_principal import identity_loaded, UserNeed, RoleNeed
from flask import url_for, session

import jwt

# from application.models import User, Customer, PasswordResetToken
from model_package import models
from model_package.signals import *

from utilities import ServiceLabs
# from application.services import search

app = models.app
# login_manager = app.login_manager
logger = app.logger

db = models.app.db

PasswordResetTokenService = ServiceLabs.create_instance(models.PasswordResetToken, db)


def encode_auth_token(user_id):
    """
    generate authorization key for api connection
    """
    try:
        payload = {
            'exp': datetime.utcnow() + timedelta(days=365),
            'iat': datetime.utcnow(),
            'sub': user_id
        }
        return jwt.encode(
            payload,
            app.config.get('SECRET_KEY'),
            algorithm='HS256'
        )
    except Exception as e:
        raise e


def decode_auth_token(token):
    """
    decode authorization token
    """
    try:
        payload = jwt.decode(token, app.config['SECRET_KEY'])
        return payload
    except jwt.ExpiredSignatureError:
        return 'Signature expired. Please log in again.'
    except jwt.InvalidTokenError:
        return 'Invalid token. Please log in again.'


# @login_manager.user_loader
def load_user(user_id):
    """
    Retrieves a user using the id stored in the session.

    :param userid: The user id (fetched from the session)
    :returns: the logged in user or None
    """
    return models.User.query.get(user_id)


@identity_loaded.connect
def on_identity_loaded(sender, identity):
    """
    Loads a current user's roles into the identity context
    """

    identity.user = current_user

    # Add the UserNeed to the identity
    if hasattr(identity.user, "id"):
        identity.provides.add(UserNeed(identity.user.id))

    # Assuming the current user has a list of roles, update the
    # identity with the roles that the user provides
    if hasattr(identity.user, "roles"):
        for role in identity.user.roles:
            identity.provides.add(RoleNeed(role.name))


def authenticate(username, password, **kwargs):
    """
    Fetch a user based on the given username and password.

    :param username: the username (or email address) of the user
    :param password: password credential
    :param kwargs: additional parameters required

    :returns: a user object or None
    """

    user = models.User.query.filter(models.User.client_id.isnot(None)).filter(models.User.email == username).first()

    if user and user.check_password(password):

        user.update_last_login()
        session["user_id"] = user.id
        if user.client:
            session['client_id'] = user.client.id
        return user
    else:
        return None


def authenticate_customer(username, password, **kwargs):
    """
    Fetch a user based on the given username and password.

    :param username: the username (or email address) of the user
    :param password: password credential
    :param kwargs: additional parameters required

    :returns: a user object or None
    """

    customer = models.Customer.query.filter(models.Customer.email == username).first()

    if customer and customer.check_password(password):

        customer.update_last_login()
        session["customer_id"] = customer.id
        session['customer_clients_id'] = [i.id for i in customer.clients]
        return customer
    else:
        return None


def authenticate_forgot_password(username, **kwargs):
    """
    """
    user = models.User.query.filter(models.User.client_id.isnot(None)).filter(models.User.email == username).first()
    if user:
        return user
    else:
        return None


def authenticate_admin(username, password, **kwargs):
    """
    Fetch a user based on the given username and password.

    :param username: the username (or email address) of the user
    :param password: password credential
    :param kwargs: additional parameters required

    :returns: a user object or None
    """
    user = models.User.query.filter(and_(models.User.is_admin == True, models.User.is_staff == True,
                                         models.User.is_active == True)).filter(or_(models.User.username == username,
                                                                                    models.User.email == username)
                                                                                ).first()
    if user and user.check_password(password):
        # search.index(user)
        return user
    else:
        return None


def request_user_password(user_id, **kwargs):
    """
    Generates a password change token for the requested user
    """
    user = models.User.query.get(user_id)
    try:
        token = PasswordResetTokenService.create(email=user.email, user_id=user.id)
        user_password_reset_requested.send(token.id)  # send signal

        return token
    except:
        db.session.rollback()
        raise


def request_customer_password(customer_id, **kwargs):
    """
    Generates a password change token for the requested user
    """
    customer = models.Customer.query.get(customer_id)

    try:
        token = PasswordResetTokenService.create(email=customer.email, customer_id=customer_id)
        customer_password_reset_requested.send(token.id)  # send signal

        return token
    except:
        db.session.rollback()
        raise


def require_basic_auth(realm="ATELE API"):
    """ Sends a 401 Authorization required response that enables basic auth """

    message = "Could not authorize your request. Provide the proper login credentials to continue."

    headers = {"WWW-Authenticate": "Basic realm='%s'" % realm}
    status = 401

    return message, status, headers  # body, status and headers


# def check_basic_auth(username, auth_token, **kwargs):
#     """
#     Fetch a user based on the given username and token key given.
#     This is used along with HTTP Basic Authentication
#
#     :param username: the username (or email address) of the user
#     :param auth_token: authentication token generated for the user
#
#     :returns: a user object or None
#
#     """
#     user = User.query.filter(or_(User.username == username, User.email == username)).first()
#     if user and auth_token == user.get_auth_token():
#         search.index(user)
#         return user
#     else:
#         return None
#
#
# def fetch_navigation(client_id, is_admin=False):
#     """ Fetches the navigation links for a current client """
#     # For now it will simply return all required links
#
#     top_def_links = [
#         {"name": "Dashboard", "url": url_for(".index"), "code": "index", "icon": "dashboard"},
#         {"name": "Homepage", "url": url_for(".homepage"), "code": "homepage", "icon": "desktop"},
#         {"name": "Messages", "url": url_for(".messages"), "code": "messages", "icon": "envelope-o"},
#         {"name": "Categories", "url": url_for(".groups"), "code": "groups", "icon": "list-alt"},
#         {"name": "Products", "url": url_for(".products"), "code": "products", "icon": "gift"},
#         {"name": "Product Custom Field", "url": url_for(".product_custom_fields"), "code": "product_custom_fields",
#          "icon": "th"},
#     ]
#
#     bottom_def_links = [
#         {"name": "Delivery", "url": url_for(".delivery"), "code": "delivery", "icon": "truck"},
#         {"name": "Inventory", "url": url_for(".variants"), "code": "variants", "icon": "leaf"},
#         {"name": "Review and Ratings", "url": url_for(".reviews"), "code": "reviews", "icon": "star"},
#         {"name": "Quick Pay", "url": url_for(".payments"), "code": "payments", "icon": "credit-card"},
#         {"name": "Transactions", "url": url_for(".transactions"), "code": "transactions", "icon": "money"},
#         {"name": "Order Carts", "url": url_for(".order_carts"), "code": "order_carts", "icon": "shopping-basket"},
#         {"name": "Orders", "url": url_for(".orders"), "code": "orders", "icon": "shopping-cart"},
#         {"name": "Refunds", "url": url_for(".refunds"), "code": "refunds", "icon": "shopping-cart mirror_icon"},
#         {"name": "Banners", "url": url_for(".banners"), "code": "banners", "icon": "picture-o"},
#         {"name": "Shop Info", "url": url_for(".info"), "code": "info", "icon": "info"},
#     ]
#
#     links = top_def_links + bottom_def_links
#
#     shop = search.get("shop", client_id)
#
#     if shop.subscription_id and shop.support_service:
#         s_link = [{"name": "Services", "url": url_for(".services"), "code": "services", "icon": "briefcase"}, ]
#
#         top_def_links = top_def_links + s_link
#
#     if shop.subscription_id and shop.support_donation:
#         d_link = [{"name": "Donations", "url": url_for(".donations"), "code": "donations", "icon": "money"}, ]
#
#         top_def_links = top_def_links + d_link
#
#     links = top_def_links + bottom_def_links
#
#     general = {
#         "links": links
#     }
#
#     return dict(nav=general)
#
#
# def check_admin_auth(username, auth_token, **kwargs):
#     """
#     Fetch a user based on the given username and token key given.
#     This is used along with HTTP Basic Authentication
#
#     :param username: the username (or email address) of the user
#     :param auth_token: authentication token generated for the user
#
#     :returns: a user object or None
#
#     """
#     user = User.query.filter(or_(User.username == username, User.email == username)).first()
#     if user and auth_token == user.get_auth_token():
#         search.index(user)
#         return user
#     else:
#         return None
#
#
# def fetch_admin_navigation(user=None):
#     """ Fetches the navigation links for a current user """
#     # For now it will simply return all required links
#
#     admin = {
#         "links": [
#             {"name": "Dashboard", "url": url_for(".index"), "code": "index", "icon": "fa-dashboard", "is_img": False,
#              "sub_links": []},
#             {"name": "Assets", "url": url_for(".assets"), "code": "assets",
#              "icon": url_for('static', filename='images/icons/assets.png'), "is_img": True, "sub_links": [
#                 {"name": "Assets", "url": url_for(".assets"), "code": "assets", "icon": "fa-cogs"},
#                 {"name": "Transformers", "url": url_for(".transformers"), "code": "transformers", "icon": "fa-cogs"},
#             ]
#              },
#             {"name": "Devices", "url": url_for(".devices"), "code": "devices",
#              "icon": url_for('static', filename='images/icons/devices.png'), "is_img": True, "sub_links": [
#                 {"name": "Devices", "url": url_for(".devices"), "code": "devices", "icon": "fa-male"},
#             ]
#              },
#             {"name": "Components", "url": url_for(".components"), "code": "components", "icon": "fa-cogs",
#              "is_img": False, "sub_links": []},
#             {"name": "Clients", "url": url_for(".clients"), "code": "clients", "icon": "fa-male", "is_img": False,
#              "sub_links": [
#                  {"name": "Clients", "url": url_for(".clients"), "code": "clients", "icon": "fa-male"},
#                  {"name": "Customers", "url": url_for(".customers"), "code": "customers", "icon": "fa-group"},
#              ]
#              },
#             {"name": "Payment Channels", "url": url_for(".payment_channels"), "code": "client_payment_channels",
#              "icon": url_for('static', filename='images/icons/payment_channel.png'), "is_img": True, "sub_links": []},
#             {"name": "Messages", "url": url_for(".admin_messages"), "code": "admin_messages", "icon": "fa-envelope-o",
#              "is_img": False, "sub_links": [
#                 {"name": "Admin Messages", "url": url_for(".admin_messages"), "code": "admin_messages",
#                  "has_badge": True, "icon": "fa-envelope-o"},
#                 {"name": "Client Messages", "url": url_for(".all_client_messages"), "code": "all_client_messages",
#                  "has_badge": True, "icon": "fa-envelope-o"},
#             ]
#              },
#             {"name": "Notifications", "url": url_for(".notifications"), "code": "notifications", "icon": "fa-bullhorn",
#              "is_img": False, "has_badge": True, "sub_links": []},
#             {"name": "Location & Currency", "url": url_for(".countries"), "code": "countries", "icon": "fa-globe",
#              "is_img": False, "sub_links": [
#                 {"name": "Countries", "url": url_for(".countries"), "code": "countries", "icon": "fa-globe"},
#                 {"name": "States", "url": url_for(".states"), "code": "states", "icon": "fa-globe"},
#                 # {"name": "Cities", "url": url_for(".cities"), "code": "cities", "icon": "fa-globe"},
#                 {"name": "Timezones", "url": url_for(".timezones"), "code": "timezones", "icon": "fa-globe"},
#                 {"name": "Currency", "url": url_for(".currencies"), "code": "currencies", "icon": "fa-globe"},
#             ]
#              },
#             {"name": "Reports", "url": url_for(".reports"), "code": "reports", "icon": "fa-bar-chart-o",
#              "is_img": False, "sub_links": []},
#             {"name": "Terms & Disclaimer", "url": url_for(".terms_disclaimer"), "code": "terms_disclaimer",
#              "icon": "fa-file-text", "is_img": False, "sub_links": []},
#             {"name": "Settings", "url": url_for(".settings_dashboard"), "code": "settings_dashboard", "icon": "fa-gear",
#              "is_img": False, "sub_links": [
#                 {"name": "Dashboard One", "url": url_for(".settings_dashboard"), "code": "settings_dashboard",
#                  "icon": "fa-gear"},
#                 {"name": "Dashboard Two", "url": url_for(".settings_dashboard_2"), "code": "settings_dashboard_2",
#                  "icon": "fa-gear"},
#                 {"name": "Dashboard Three", "url": url_for(".settings_dashboard_3"), "code": "settings_dashboard_3",
#                  "icon": "fa-gear"},
#                 {"name": "Dashboard Four", "url": url_for(".settings_dashboard_4"), "code": "settings_dashboard_4",
#                  "icon": "fa-gear"},
#                 {"name": "Lists", "url": url_for(".settings_list"), "code": "settings_list", "icon": "fa-gear"},
#                 {"name": "Forms", "url": url_for(".settings_form"), "code": "settings_form", "icon": "fa-gear"}
#             ]
#              }
#         ]
#     }
#
#     return dict(nav=admin)


def _client_navigation():
    """ Generates client navigation based on id """
    return [
        {"name": "Home", "url": "home", "code": "index",
         "icon": "fa-dashboard", "is_img": False, "sub_links": []
         },
        {"name": "Devices", "url": "devices", "code": "client_devices",
         "icon": url_for('static', filename='images/icons/devices.png'), "is_img": True, "sub_links": [
            {"name": "Device List", "url": "#!/devices", "code": "device_list",
             "icon": "fa-male"},
            {"name": "Add Device", "url": "#!/devices/new",
             "code": "devices_new", "icon": "fa-male"}
        ]
         },
        {"name": "Mesh", "url": "meshes", "code": "client_mesh_dashboard",
         "icon": url_for('static', filename='images/icons/mesh.png'), "is_img": True, "sub_links": [
            {"name": "Dashboard", "url": "#!/meshes",
             "code": "client_mesh_dashboard", "icon": "fa-group"},
            {"name": "Meshes", "url": "#!/meshes/list", "code": "meshes_list",
             "icon": url_for('static', filename='images/icons/mesh.png'), "is_img": True},
            {"name": "Add Mesh", "url": "#!/meshes/new", "code": "meshes_new",
             "icon": url_for('static', filename='images/icons/mesh.png'), "is_img": True},
        ]
         },
        {"name": "Installations", "url": "installations", "code": "client_installations",
         "icon": url_for('static', filename='images/icons/devices.png'), "is_img": True, "sub_links": [
            {"name": "Transformers", "url": "#!/installations/transformers",
             "code": "client_transformers", "icon": "fa-male"},
            # {"name": "LV Panels", "url": "#!/installations/lv", "code": "client_lv_panels",
            #  "icon": "fa-male"},
            # {"name": "MV Panels", "url": "#!/installations/mv", "code": "client_mv_panels",
            #  "icon": "fa-male"},
            {"name": "Panels", "url": "#!/installations/panels", "code": "client_panels",
             "icon": "fa-male"},
            {"name": "Switch Gears", "url": "#!/installations/switchgears",
             "code": "client_switch_gears", "icon": "fa-male"},
            {"name": "Line Connectors", "url": "#!/installations/lines",
             "code": "client_line_connectors", "icon": "fa-male"},
        ]
         },
        {"name": "Facilities/Stations", "url": "stations", "code": "client_stations",
         "icon": url_for('static', filename='images/icons/devices.png'), "is_img": True, "sub_links": [
            {"name": "Stations List", "url": "#!/stations",
             "code": "stations_list", "icon": "fa-male"},
            {"name": "Add Station", "url": "#!/stations/new", "code": "stations_new",
             "icon": "fa-male"}
        ]
         },
        {"name": "Payments", "url": "payments", "code": "client_billings",
         "icon": url_for('static', filename='images/icons/payments.png'), "is_img": True, "sub_links": [
            {"name": "Billing Information", "url": "#!/payments/billings",
             "code": "client_billings", "icon": "fa-group"},
            {"name": "Wallets", "url": "#!/payments/wallets", "code": "client_wallets",
             "icon": "fa-group"},
        ]
         },
        {"name": "Customers", "url": 'customers', "code": "client_customers", "icon": "fa-users", "is_img": False,
         "sub_links": [
             {"name": "Dashboard", "url": '#!/customers',
              "code": "client_customer_dashboard", "icon": "fa-group"},
             {"name": "Customers List", "url": '#!/customers/list', "code": "client_customers_list",
              "icon": "fa-book"},
             {"name": "Add Customer", "url": '#!/customers/new', "code": "customers_new",
              "icon": "fa-book"},
         ]
         },
        # {"name": "Complaints", "url": url_for(".client_complaints", client_id=client_id), "code": "client_complaints",
        #  "icon": url_for('static', filename='images/icons/complaints.png'), "is_img": True, "sub_links": []},
        # {"name": "Requests", "url": url_for(".client_requests", client_id=client_id), "code": "client_requests",
        #  "icon": url_for('static', filename='images/icons/requests.png'), "is_img": True, "sub_links": []},
        # {"name": "Notifications", "url": url_for(".client_notifications", client_id=client_id),
        #  "code": "client_notifications", "icon": "fa-bullhorn", "is_img": False, "sub_links": []},
        # {"name": "Supports", "url": url_for(".client_support_dashboard", client_id=client_id),
        #  "code": "client_support_dashboard", "icon": url_for('static', filename='images/icons/supports.png'),
        #  "is_img": True, "sub_links": [
        #     {"name": "Dashboard", "url": url_for(".client_support_dashboard", client_id=client_id),
        #      "code": "client_support_dashboard", "icon": "fa-group"},
        #     {"name": "Tickets", "url": url_for(".client_tickets", client_id=client_id), "code": "client_tickets",
        #      "icon": "fa-group"},
        # ]
        #  },
        # {"name": "Help", "url": url_for(".client_help", client_id=client_id), "code": "client_help",
        #  "icon": url_for('static', filename='images/icons/help.png'), "is_img": True, "sub_links": []},
        # {"name": "FAQ", "url": url_for(".client_faq", client_id=client_id), "code": "client_faq", "icon": "fa-bullhorn",
        #  "is_img": False, "has_badge": True, "sub_links": []},
        {"name": "Reports", "url": url_for('.reports'), "code": "client_report_dashboard", "icon": "fa-bar-chart-o",
         "is_img": False, "sub_links": [
            {"name": "Dashboard", "url": url_for('.reports'),
             "code": "client_report_dashboard", "icon": "fa-group"},
            {"name": "Devices", "url": url_for('.reports'),
             "code": "client_device_report", "icon": "fa-group"},
            {"name": "Payments", "url": url_for('.reports'),
             "code": "client_payment_report", "icon": "fa-group"},
            {"name": "Customers", "url": url_for('.reports'),
             "code": "client_customer_report", "icon": "fa-group"},
        ]
         }
    ]


def service_navigation():
    """ Generates client navigation based on id """
    return [
        {"name": "Dashboard", "url": "home", "code": "dashboard",
         "icon": "fa-dashboard", "is_img": False, "sub_links": []
         },
        {"name": "Mesh", "url": "meshes", "code": "client_mesh_dashboard",
         "icon": url_for('static', filename='images/icons/mesh.png'), "is_img": True,
         "sub_links": [
             {"name": "Dashboard", "url": "meshes",
              "code": "client_mesh_dashboard", "icon": "fa-group"},
             {"name": "Mesh", "url": "mesh", "code": "client_mesh",
              "icon": url_for('static', filename='images/icons/mesh.png'), "is_img": True},
         ]
         },
        # {"name": "Devices", "url": url_for(".client_meters", client_id=client_id), "code": "client_meters",
        #  "icon": url_for('static', filename='images/icons/devices.png'), "is_img": True, "sub_links": [
        #     {"name": "Meters", "url": url_for(".client_meters", client_id=client_id), "code": "client_meters",
        #      "icon": "fa-male"},
        #     {"name": "Transformers", "url": url_for(".client_transformers", client_id=client_id),
        #      "code": "client_transformers", "icon": "fa-male"},
        #     {"name": "LV Panels", "url": url_for(".client_lv_panels", client_id=client_id), "code": "client_lv_panels",
        #      "icon": "fa-male"},
        #     {"name": "MV Panels", "url": url_for(".client_mv_panels", client_id=client_id), "code": "client_mv_panels",
        #      "icon": "fa-male"},
        #     {"name": "Switch Gears", "url": url_for(".client_switch_gears", client_id=client_id),
        #      "code": "client_switch_gears", "icon": "fa-male"},
        #     {"name": "Line Connectors", "url": url_for(".client_line_connectors", client_id=client_id),
        #      "code": "client_line_connectors", "icon": "fa-male"},
        # ]
        #  },
        # {"name": "Payments", "url": url_for(".client_billings", client_id=client_id), "code": "client_billings",
        #  "icon": url_for('static', filename='images/icons/payments.png'), "is_img": True, "sub_links": [
        #     {"name": "Billing Information", "url": url_for(".client_billings", client_id=client_id),
        #      "code": "client_billings", "icon": "fa-group"},
        #     {"name": "Wallets", "url": url_for(".client_wallets", client_id=client_id), "code": "client_wallets",
        #      "icon": "fa-group"},
        # ]
        #  },
        # {"name": "Customers", "url": url_for(".client_customer_dashboard", client_id=client_id),
        #  "code": "client_customer_dashboard", "icon": "fa-male", "is_img": False, "sub_links": [
        #     {"name": "Dashboard", "url": url_for(".client_customer_dashboard", client_id=client_id),
        #      "code": "client_customer_dashboard", "icon": "fa-group"},
        #     {"name": "Customers", "url": url_for(".client_customers", client_id=client_id), "code": "client_customers",
        #      "icon": "fa-group"},
        # ]
        #  },
        # {"name": "Complaints", "url": url_for(".client_complaints", client_id=client_id), "code": "client_complaints",
        #  "icon": url_for('static', filename='images/icons/complaints.png'), "is_img": True, "sub_links": []},
        # {"name": "Requests", "url": url_for(".client_requests", client_id=client_id), "code": "client_requests",
        #  "icon": url_for('static', filename='images/icons/requests.png'), "is_img": True, "sub_links": []},
        # {"name": "Notifications", "url": url_for(".client_notifications", client_id=client_id),
        #  "code": "client_notifications", "icon": "fa-bullhorn", "is_img": False, "sub_links": []},
        # {"name": "Supports", "url": url_for(".client_support_dashboard", client_id=client_id),
        #  "code": "client_support_dashboard", "icon": url_for('static', filename='images/icons/supports.png'),
        #  "is_img": True, "sub_links": [
        #     {"name": "Dashboard", "url": url_for(".client_support_dashboard", client_id=client_id),
        #      "code": "client_support_dashboard", "icon": "fa-group"},
        #     {"name": "Tickets", "url": url_for(".client_tickets", client_id=client_id), "code": "client_tickets",
        #      "icon": "fa-group"},
        # ]
        #  },
        # {"name": "Help", "url": url_for(".client_help", client_id=client_id), "code": "client_help",
        #  "icon": url_for('static', filename='images/icons/help.png'), "is_img": True, "sub_links": []},
        # {"name": "FAQ", "url": url_for(".client_faq", client_id=client_id), "code": "client_faq", "icon": "fa-bullhorn",
        #  "is_img": False, "has_badge": True, "sub_links": []},
        # {"name": "Reports", "url": url_for(".client_report_dashboard", client_id=client_id),
        #  "code": "client_report_dashboard", "icon": "fa-bar-chart-o", "is_img": False, "sub_links": [
        #     {"name": "Dashboard", "url": url_for(".client_report_dashboard", client_id=client_id),
        #      "code": "client_report_dashboard", "icon": "fa-group"},
        #     {"name": "Devices", "url": url_for(".client_device_report", client_id=client_id),
        #      "code": "client_device_report", "icon": "fa-group"},
        #     {"name": "Payments", "url": url_for(".client_payment_report", client_id=client_id),
        #      "code": "client_payment_report", "icon": "fa-group"},
        #     {"name": "Customers", "url": url_for(".client_customer_report", client_id=client_id),
        #      "code": "client_customer_report", "icon": "fa-group"},
        # ]
        #  },
        {
            "name": "Settings", "url": '#!/settings', "code": "settings", "angular": True, "icon": "fa-gears"
        },
        {
            "name": "Support", "url": app.config['SUPPORT_URL'], "code": "info", "angular": False,
            "icon": "fa-life-ring"
        }
    ]


def anonymous_user(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if current_user.is_authenticated:
            return redirect(url_for('.index'))
        return func(*args, **kwargs)

    return decorated_view

