from flask import current_app as app

with app.app_context():
    from utilities import ServiceLabs, clean_kwargs
    from model_package import models
    # from application import models

    db = models.app.db

    AddressService = ServiceLabs.create_instance(models.Address, db)
    GeojsonAddressService = ServiceLabs.create_instance(models.GeojsonAddress, db)
    BaseGeojsonPointService = ServiceLabs.create_instance(models.GeojsonPoint, db)
    CityService = ServiceLabs.create_instance(models.City, db)
    StateService = ServiceLabs.create_instance(models.State, db)
    CountryService = ServiceLabs.create_instance(models.Country, db)
    TimezoneService = ServiceLabs.create_instance(models.Timezone, db)
    CurrencyService = ServiceLabs.create_instance(models.Currency, db)
    LocationService = ServiceLabs.create_instance(models.Location, db)
    BankService = ServiceLabs.create_instance(models.Bank, db)
    DefaultService = ServiceLabs.create_instance(models.Service, db)
    SettingsService = ServiceLabs.create_instance(models.Settings, db)
    CustomerSettingsService = ServiceLabs.create_instance(models.CustomerSettings, db)
    BaseInstallationTypeService = ServiceLabs.create_instance(models.InstallationType, db)
    AlertTypeService = ServiceLabs.create_instance(models.AlertType, db)
    BaseProductService = ServiceLabs.create_instance(models.Product, db)
    ReportService = ServiceLabs.create_instance(models.Report, db)


    class ProductService(BaseProductService):
        """
        custom modification to product service creation
        """

        @classmethod
        def create(cls, ignored=None, **kwargs):
            """
            custom create method
            :param ignored:
            :param kwargs:
            :return: user model object
            """
            if not ignored:
                ignored = ["id", "date_created", "last_updated"]

            try:
                data = clean_kwargs(ignored, kwargs)
                # conn.create_record('product.product', **data)
                obj = BaseProductService.create(ignored, **data)
                return obj
            except Exception as e:
                raise e


    class InstallationTypeService(BaseInstallationTypeService):
        @classmethod
        def create(cls, ignored=None, **kwargs):
            if not ignored:
                ignored = ["id", "date_created", "last_updated"]

            data = clean_kwargs(ignored, kwargs)
            service_codes = list()
            if 'services' in data:
                service_codes = data.pop('services')

            obj = BaseInstallationTypeService.create(ignored, **data)

            merged_obj = db.session.merge(obj)

            if len(service_codes) > 0:
                try:
                    services = models.Service.query.filter(models.Service.code.in_(service_codes)).all()
                    for src in services:
                        merged_src = db.session.merge(src)
                        merged_obj.services.append(merged_src)

                    db.session.add(merged_obj)
                    db.session.commit()
                except Exception, e:
                    db.session.rollback()
                    raise e

            return obj


    class GeojsonPointService(BaseGeojsonPointService):
        @classmethod
        def create(cls, ignored=None, **kwargs):

            if not ignored:
                ignored = ["id", "date_created", "last_updated"]

            data = clean_kwargs(ignored, kwargs)
            longitude = data['longitude']
            latitude = data['latitude']

            obj = BaseGeojsonPointService.create(ignored, **{
                'point': 'SRID=4326;POINT(%s %s)' % (longitude, latitude),
                'longitude': longitude,
                'latitude': latitude
            })

            return obj