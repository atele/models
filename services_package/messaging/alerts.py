from flask import current_app as app, render_template_string

from integrations_package.storage import mongo
# from solidwebpush import Pusher

from utilities import Payload
from services_package import equipment, account
from services_package.messaging import email


class FaultService(object):

    @classmethod
    def record_fault(cls, client_id, message, alert_code, level, timestamp):
        """
        record notifications
        :param client_id:
        :param level: WARNING|CRITICAL
        :param timestamp:
        :return:
        """
        try:
            alert = equipment.AlertService.filter_by(code=alert_code)
            fault_id = 'fault:{}:{}:{}'.format(client_id, level, timestamp)
            mongo.MongoService.record_data('faults', fault_id, **{
                'timestamp': timestamp,
                'body': message,
                'device': alert.device.code,
                'level': level,
                'title': '{} - {}'.format(level.upper(), alert.device.code),
                'is_resolved': False,
                'client_id': client_id,
                'installation_id': alert.device.installation_id,
                'station_id': alert.device.station_id
            })
            # issue alert
            return cls.issue_alert(alert.id, fault_id)
        except Exception, e:
            app.log.error('[ALERT: %s][LEVEL: %s][TIMESTAMP: %s][STATUS: FAILED]' % (alert_code, level, timestamp))
            app.log.error(e)
            return False

    @staticmethod
    def resolve_fault(identifier, **kwargs):
        """
        resolve a notification alert
        :param identifier
        """
        try:
            resolution = {'is_resolved': True}
            resolution.update(kwargs)
            return mongo.MongoService.update_record('faults', identifier, **resolution)
        except Exception as e:
            app.log.error('[IDENTIFIER: %s][ACTION: RESOLVE][STATUS: FAILED]' % identifier)
            app.log.error(e)
            return False

    @staticmethod
    def issue_alert(alert_id, fault_id):
        """
        issue alert to recipients
        """
        alert = equipment.AlertService.get(alert_id)
        push_recipients = filter(lambda x: x.is_push, alert.recipients)
        email_recipients = filter(lambda c: c.is_email, alert.recipients)
        device_recipients = filter(lambda d: d.is_device, alert.recipients)

        send_push_alert.delay(alert.client_id, fault_id, push_recipients)
        send_email_alert.delay(alert.client_id, fault_id, email_recipients)

        return True


@app.celery.task
def send_push_alert(client_id, fault_id, recipients):

    public_key = app.config['PUSH_PUBLIC_KEY']

    fault = mongo.MongoService.query_collection('faults', **{'id': fault_id})

    subscriptions = [Payload(c) for c in mongo.MongoService.query_collection('subscriptions',
                                                                             **{'client_id': client_id})]
    filtered = filter(lambda y: y.user_id in recipients, subscriptions)
    # push = Pusher(public_key)
    # push.sendNotificationToAll([getattr(c, 'endpoint') for c in filtered], fault)

    # return push
    return {}


@app.celery.task
def send_email_alert(client_id, fault_id, email_recipients):
    """
    send email alert to recipients
    """

    client = account.ClientService.get(client_id)
    fault = Payload(mongo.MongoService.query_collection('faults', **{'id': fault_id}))
    message = getattr(fault, 'body')
    html = render_template_string('emails/alert.html', **locals())
    return email.send_email(to=email_recipients, subject='Fault Alert', html=html)


@app.celery.task
def send_device_alert(client_id, fault_id, device_recipients):
    """
    publish alert to devices
    """
    pass

