import unicodedata

from flask import render_template
from messagerie import AWSEntity
from model_package import models

from services_package.messaging import *
# from application import models
from model_package.signals import *


@celery.task(queue='messages')
def send_email(sender="alert@atele.org", to=list(), cc=list(), bcc=list(), subject="", html="", files=None):
    """
    Proxy email function
    """

    _dev_mode = app.config.get("EMAIL_DEV_ONLY", False)
    _dev_emails = app.config.get("DEV_EMAILS", [])

    if _dev_mode:
        to = _dev_emails
        cc = []

    html = unicodedata.normalize('NFKD', html).encode('ascii', 'ignore')

    if files:
        AWSEntity.send_raw_message(sender, subject=subject, body=html, recipients=to, cc=cc, file_paths=files,
                                   is_html=True)
    else:
        AWSEntity.send_raw_message(sender, subject=subject, body=html, recipients=to, cc=cc, is_html=True)


@card_added.connect
def _card_added(card_id, **kwargs):
    """ Sends out an email when the card record has been added """
    card = models.Card.query.get(card_id)
    send_email.delay()


@invoice_payment_notification.connect
def _invoice_payment_notification(obj_id, **kwargs):
    sender = "alert@atele.org"
    invoice = models.Invoice.query.get(obj_id)
    if not invoice:
        raise Exception("Invoice passed to email payment notification does not exist")
    client = invoice.client
    to = [client.email]
    cc = ['info@atele.org']
    subject = "Payment Notification for the Month of %s" % invoice.month
    html = render_template('email/invoice.html', **locals())
    send_email(sender=sender, to=to, cc=cc, subject=subject, html=html)


def test_send_email():
    sender = "emails@atele.org"
    to = ['ademola@atele.org', 'kunle@officemotion.net', 'kunsam002@yahoo.com', 'kunle@atele.org', 'styccs@gmail.com']
    cc = ['ademola@atele.org', 'kunle@officemotion.net', 'kunsam002@yahoo.com', 'kunle@atele.org', 'styccs@gmail.com']
    list = [
        {"subject": "Welcome To Atele.org", "path": "email/welcome.html"},
        {"subject": "Upgrades on Atele", "path": "email/upsell.html"},
        {"subject": "Survey from Atele", "path": "email/survey.html"},
        {"subject": "Progress On Atele", "path": "email/progress.html"},
        {"subject": "Ping from Atele", "path": "email/ping.html"},
        {"subject": "Invoice from Atele", "path": "email/invoice.html"},
        {"subject": "Invitation to Atele", "path": "email/invite.html"},
        {"subject": "Confirmation from Atele", "path": "email/confirm.html"},
        {"subject": "Missing You at Atele", "path": "email/reignite.html"}
    ]
    for i in list:
        subject = "%s"%i.get("subject")
        html = render_template('%s'%i.get('path'))
        send_email(sender=sender, to=to, cc=cc, subject=subject, html=html)
    # send_email.delay(sender=sender,to=to,cc=cc,subject=subject,html=html)


@customer_account_created.connect
def _customer_account_created(token_id, password, **kwargs):

    token = models.PasswordResetToken.query.get(token_id)

    if not token:
        raise Exception("Token not found")

    sender = "alert@atele.org"
    to = [token.email]
    cc = []
    subject = "New Customer Account Notification"

    customer = models.Customer.query.get(token.customer_id)
    html = render_template('email/new_customer.html', **locals())

    send_email(sender=sender, to=to, cc=cc, subject=subject, html=html)


@user_password_reset_requested.connect
def _user_password_reset_requested(token_id, **kwargs):

    token = models.PasswordResetToken.query.get(token_id)

    if not token:
        raise Exception("Token not found")

    sender = "alert@atele.org"
    to = [token.email]
    cc = []
    subject = "Password Reset Notification"

    user = models.User.query.get(token.user_id)
    html = render_template('email/forgot_password.html', **locals())

    send_email(sender=sender, to=to, cc=cc, subject=subject, html=html)


@customer_password_reset_requested.connect
def _customer_password_reset_requested(token_id, **kwargs):

    token = models.PasswordResetToken.query.get(token_id)

    if not token:
        raise Exception("Token not found")

    sender = "alert@atele.org"
    to = [token.email]
    cc = []
    subject = "Password Reset Notification"

    customer = models.Customer.query.get(token.customer_id)
    html = render_template('email/forgot_password.html', **locals())

    send_email(sender=sender, to=to, cc=cc, subject=subject, html=html)

