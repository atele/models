import time

from flask import current_app as app, render_template

from shapely import geometry, wkb

from integrations_package.storage import mongo
from utilities import Payload, id_generator, compute_lat_lng, ServiceLabs, clean_kwargs, generate_code, generate_uuid, \
    ObjectNotFoundException, populate_obj, exceptions
from model_package import models

with app.app_context():
    import payment, account, basic
    from messaging import email

    db = models.app.db

    DeviceTypeService = ServiceLabs.create_instance(models.DeviceType, db)
    BaseDeviceService = ServiceLabs.create_instance(models.Device, db)
    BaseNetworkService = ServiceLabs.create_instance(models.Network, db)
    BaseInstallationService = ServiceLabs.create_instance(models.Installation, db)
    BaseStationService = ServiceLabs.create_instance(models.Station, db)
    BasicAlertService = ServiceLabs.create_instance(models.Alert, db)
    AlertRecipientService = ServiceLabs.create_instance(models.AlertRecipient, db)


class StationService(BaseStationService):
    """
    Custom modification to base substation service
    """

    @classmethod
    def create(cls, ignored=None, **kwargs):
        """
        custom create method
        :param ignored:
        :param kwargs:
        :return: user model object
        """
        if not ignored:
            ignored = ["id", "date_created", "last_updated"]

        data = clean_kwargs(ignored, kwargs)

        location = basic.LocationService.create(ignored, **data)
        merged_location = db.session.merge(location)

        coordinates = compute_lat_lng(location.address)
        if coordinates is not None and len(coordinates) == 2:
            merged_location.latitude = coordinates[1]
            merged_location.longitude = coordinates[0]
            db.session.add(merged_location)
            db.session.commit()

        data.update({'location_id': merged_location.id})

        obj = BaseStationService.create(ignored, **data)

        setting = basic.SettingsService.filter_by(client_id=obj.client_id)

        if not setting.stations_dep_resolved:
            setting.stations_dep_resolved = True
            current_db_session = cls.conn.object_session(setting)
            current_db_session.add(setting)
            current_db_session.commit()

        network_data = {
            'code': '{}:{}:{}'.format(obj.id, time.time(), id_generator(5)),
            'station_id': obj.id,
            'client_id': obj.client_id,
        }
        NetworkService.create(ignored, **network_data)

        code = obj.client.name.replace(" ","")[:3].upper() + str(obj.id).zfill(3) + id_generator(size=4)
        obj = BaseStationService.update(obj.id, code=code)

        return obj

    @classmethod
    def update(cls, obj_id, ignored=None, **kwargs):
        """
        update station/facility
        :param obj_id:
        :param ignored:
        :param kwargs:
        :return:
        """
        _obj = cls.query.get(obj_id)

        if not _obj:
            raise ObjectNotFoundException(cls.model_class, obj_id)

        if not ignored:
            ignored = ["id", "date_created", "last_updated"]

        data = clean_kwargs(ignored, kwargs)

        # update location
        location = _obj.location
        location = populate_obj(location, data)
        current_db_session = cls.conn.object_session(location)
        current_db_session.add(location)
        current_db_session.commit()

        # update station
        obj = populate_obj(_obj, data)
        current_db_session = cls.conn.object_session(obj)
        current_db_session.add(obj)
        current_db_session.commit()

        setting = basic.SettingsService.filter_by(client_id=obj.client_id)

        if not setting.stations_dep_resolved:
            setting.stations_dep_resolved = True
            current_db_session = cls.conn.object_session(setting)
            current_db_session.add(setting)
            current_db_session.commit()

        return obj

    @classmethod
    def active_view_query(cls, query):
        return query.filter(models.Station.is_active==True)

    @classmethod
    def in_active_view_query(cls, query):
        return query.filter(models.Station.is_active!=True)


class InstallationService(BaseInstallationService):
    @classmethod
    def create(cls, ignored=None, **kwargs):
        """
        returns installation object instance
        :param ignored:
        :param kwargs:
        :return:
        """
        if not ignored:
            ignored = ["id", "date_created", "last_updated"]

        data = clean_kwargs(ignored, kwargs)

        service = basic.DefaultService.get(data['service_id'])

        if not service:
            raise ObjectNotFoundException(basic.DefaultService.model_class, data['service_id'])

        client = account.ClientService.get(data['client_id'])

        if not client:
            raise ObjectNotFoundException(account.ClientService.model_class, data['client_id'])

        station = StationService.get(data['station_id'])

        if not station:
            raise ObjectNotFoundException(StationService.model_class, data['station_id'])

        product = basic.ProductService.get(data['product_id'])

        if not product:
            raise ObjectNotFoundException(basic.ProductService.model_class, data['product_id'])

        # set installation dependency as true
        subscription = payment.SubscriptionService.filter_by(client_id=client.id, service_id=service.id)

        if not subscription:
            raise ObjectNotFoundException(payment.SubscriptionService.model_class, subscription.id)

        code = generate_code(subscription.service.code, 6)
        data.update(dict(code=code))

        model_number = data.get('model_number')

        installation = BaseInstallationService.filter_by(model_number=model_number, client_id=client.id,
                                                         station_id=station.id)

        if installation:
            print(exceptions.CustomException(422, description='Installation with model number - {} and client id - {} already '
                                                   'exists'.format()))
            raise exceptions.CustomException(422, description='Installation with model number - {} and client id - {} already '
                                                   'exists'.format())

        installation = BaseInstallationService.create(ignored, **data)

        if not subscription.inst_dep_resolved:
            subscription.inst_dep_resolved = True
            current_db_session = cls.conn.object_session(subscription)
            current_db_session.add(subscription)
            current_db_session.commit()

        # request device attached to this service that is on default
        DeviceService.request_device(installation_id=installation.id, product_id=product.id)
        merged_station = db.session.merge(station)

        # associate selected service to station
        if service not in station.station_services:
            merged_service = db.session.merge(service)
            merged_station.station_services.append(merged_service)
            db.session.add(merged_station)
            db.session.commit()

        merged_installation = db.session.merge(installation)
        return merged_installation


class AlertService(BasicAlertService):
    """
    Alert service
    """

    @classmethod
    def create(cls, ignored=None, **kwargs):
        """
        custom create method
        :param ignored:
        :param kwargs:
        :return: alert model object
        """
        if not ignored:
            ignored = ["id", "date_created", "last_updated"]

        data = clean_kwargs(ignored, kwargs)
        subscription = payment.SubscriptionService.filter_by(client_id=data['client_id'], service_id=data['service_id'])
        if not subscription:
            raise ObjectNotFoundException(payment.SubscriptionService.model_class,
                                          '(client_id - {}), (service_id - {})'.format(data['client_id'],
                                                                                       data['service_id']))
        data.update(dict(code=generate_uuid()))

        data['is_warning'] = data.get('level') == 'warning'
        data['is_critical'] = data.get('level') == 'critical'

        obj = BasicAlertService.create(ignored, **data)

        station = obj.station
        recipient_data = dict(alert_id=obj.id, recipient=station.manager.email, is_email=data.get('is_email', True),
                              is_sms=data.get('is_sms', False), is_device=data.get('is_device', False),
                              client_id=obj.client_id)

        recipient = AlertRecipientService.create(ignored, **recipient_data)

        if not recipient:
            raise Exception('Alert Recipients not created')

        if not subscription.alerts_dep_resolved:
            subscription.alerts_dep_resolved = True
            current_db_session = cls.conn.object_session(subscription)
            current_db_session.add(subscription)
            current_db_session.commit()

        return obj

    @classmethod
    def update(cls, obj_id, ignored=None, **kwargs):
        """
        update station/facility
        :param obj_id:
        :param ignored:
        :param kwargs:
        :return:
        """
        _obj = cls.query.get(obj_id)

        if not _obj:
            raise ObjectNotFoundException(cls.model_class, obj_id)

        if not ignored:
            ignored = ["id", "date_created", "last_updated"]

        data = clean_kwargs(ignored, kwargs)

        # update station
        obj = populate_obj(_obj, data)
        current_db_session = cls.conn.object_session(obj)
        current_db_session.add(obj)
        current_db_session.commit()

        subscription = payment.SubscriptionService.filter_by(client_id=obj.client_id)

        if not subscription.alerts_dep_resolved:
            subscription.alerts_dep_resolved = True
            current_db_session = cls.conn.object_session(subscription)
            current_db_session.add(subscription)
            current_db_session.commit()

            # check if is_device
            # then requisition for device
        return obj


@app.celery.task
def request_device(timestamp, identifier, message, **kwargs):
    resp = mongo.MongoService.record_data("device_requests", identifier, timestamp, **kwargs)

    # send email to admin
    message = message
    config = app.config
    email.send_email(config['DEVICE_REQUESTS']['SENDER'], [config['DEVICE_REQUESTS']['RECIPIENT']],
                     subject='Device request', html=render_template("email/device_request.html", **locals()))

    return True


class DeviceService(BaseDeviceService):
    @staticmethod
    def request_device(installation_id, product_id, network_id=None):
        timestamp = time.time()
        installation = InstallationService.get(installation_id)
        data = {
            'client_id': installation.client_id,
            'station_id': installation.station_id,
            'installation_id': installation.id,
            'product_id': product_id,
            'network_id': network_id,
            'is_network': False,
            'is_alert': False,
            'is_delivered': False
        }
        identifier = '{}:{}:{}:device-request'.format(installation.client_id, installation.station_id, timestamp)
        client = account.ClientService.get(installation.client_id)
        station = StationService.get(installation.station_id)
        message = '{} just requested for a device for station - {}. IDENTIFIER - {}'.format(
            client.name, station.name, identifier)

        return request_device(timestamp, identifier, message, **data)

    @staticmethod
    def deliver_device(identifier, code, **coordinates):
        """
        deliver device to client
        """
        req = mongo.MongoService.query_collection("device_requests", first_only=True, **{"id": identifier})
        device_req = Payload(**req)
        product = basic.ProductService.get(getattr(device_req, 'product_id'))
        client = account.ClientService.get(getattr(device_req, 'client_id'))
        station = StationService.get(getattr(device_req, 'station_id'))
        installation = InstallationService.get(getattr(device_req, 'installation_id'))

        longitude = coordinates.get('longitude', installation.location.longitude)
        latitude = coordinates.get('latitude', installation.location.latitude)

        point = basic.GeojsonPointService.create(longitude=longitude, latitude=latitude)

        data = {
            'code': code,
            'station_id': station.id,
            'client_id': client.id,
            'product_id': product.id,
            'installation_id': installation.id,
            'point_id': point.id
        }

        if getattr(device_req, 'network_id'):
            data['network_id'] = getattr(device_req, 'network_id')

        device = DeviceService.create(**data)
        if device:
            mongo.MongoService.update_record("device_requests", identifier, **{'is_delivered': True})

        if device.network_id:
            network = NetworkService.get(device.network_id)
            devices = list()
            for dev in network.devices:
                point = wkb.loads(bytes(dev.point.point.data))
                devices.append(point)

            if len(devices) >= 3:
                polygon = geometry.Polygon([[p.x, p.y] for p in devices])
                network.polygon = polygon
                models.db.session.add(network)
                models.db.session.commit()

        return device


class NetworkService(BaseNetworkService):
    @classmethod
    def create(cls, ignored=None, **kwargs):
        """
        custom network network create method
        """
        if not ignored:
            ignored = ["id", "date_created", "last_updated"]

        data = clean_kwargs(ignored, kwargs)

        station = StationService.get(data['station_id'])

        if not station:
            raise ObjectNotFoundException(StationService, data['station_id'])

        data['code'] = '{}:{}:{}'.format(station.id, time.time(), id_generator(5))
        obj = BaseNetworkService.create(ignored, **data)

        # request Network device
        product = basic.ProductService.filter_by(is_network=True, on_default=True)
        NetworkService.request_device(client_id=obj.client_id, station_id=obj.station_id, product_id=product.id,
                                      network_id=obj.id)

        merged_obj = db.session.merge(obj)

        return merged_obj

    @staticmethod
    def request_device(client_id, station_id, product_id, network_id=None):
        timestamp = time.time()
        data = {
            'client_id': client_id,
            'station_id': station_id,
            'product_id': product_id,
            'network_id': network_id,
            'is_network': True,
            'is_alert': False,
            'is_delivered': False
        }
        identifier = '{}:{}:{}:network-request'.format(client_id, station_id, timestamp)
        client = account.ClientService.get(client_id)
        station = StationService.get(station_id)
        message = '{} just requested for a network/gateway device for station - {}. IDENTIFIER - {}'.format(
            client.name, station.name, identifier)

        return request_device.delay(timestamp, identifier, message, **data)

    @staticmethod
    def deliver_device(identifier, code, **coordinates):
        """
        deliver device to client
        """
        req = mongo.MongoService.query_collection("device_requests", first_only=True, **{"id": identifier})
        device_req = Payload(**req)
        product = basic.ProductService.get(getattr(device_req, 'product_id'))
        client = account.ClientService.get(getattr(device_req, 'client_id'))
        station = StationService.get(getattr(device_req, 'station_id'))
        network = NetworkService.get(getattr(device_req, 'network_id'))

        longitude = coordinates.get('location', station.location.longitude)
        latitude = coordinates.get('location', station.location.latitude)
        point = basic.GeojsonPointService.create(longitude=longitude, latitude=latitude)

        data = {
            'code': code,
            'station_id': station.id,
            'client_id': client.id,
            'product_id': product.id,
            'network_id': network.id,
            'point_id': point.id,
            'is_coordinator': True,
            'is_node': True if len(product.metrics) > 0 else False
        }
        device = DeviceService.create(**data)

        merged_net = NetworkService.get(network.id)
        devices = list()
        for dev in merged_net.devices:
            point = wkb.loads(bytes(dev.point.point.data))
            devices.append(point)

        if len(devices) >= 3:
            polygon = geometry.Polygon([[p.x, p.y] for p in devices])
            network.polygon = polygon
            models.db.session.add(network)
            models.db.session.commit()

        if network:
            mongo.MongoService.update_record("device_requests", identifier, **{'is_delivered': True})

        return device
