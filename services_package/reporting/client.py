from flask import current_app as app
from sqlalchemy import func, asc, desc
from services_package.reporting import create_report, ReportGen
from datetime import datetime, timedelta

from model_package import models

celery = app.celery
db = models.app.db


@celery.task(queue="reports")
def users_report(client_id, start, data_type, just_data, period=1):
    """ Generates a list of users that registered within the specified period """

    handle = "client_users"

    end = start + timedelta(days=period)

    query = models.User.query.filter(models.User.date_created >= start, models.User.date_created < end)

    results = query.all()

    headers = ["date | time", "name", "email", "is_active", "is_verified", "client"]

    # create the workbook
    filename = "%s-%s-to-%s" % ("%s-report"%handle, str(start.date()), str(end.date()))

    # Next write the content. Starting from the second row

    data = []
    for idx, obj in enumerate(results):
        item = (
            obj.date_created.strftime("%d-%m-%Y | %H:%M"),
            obj.name,
            obj.email,
            obj.is_active,
            obj.is_verified,
            obj.client.name if obj.client else ""
        )

        data.append(item)

    if data_type == "csv":
        generated = ReportGen.download_csv(filename, headers, *data)
    elif data_type == "xlsx":
        generated = ReportGen.download_xlsx(filename, headers, *data)
    elif data_type == "json":
        generated = ReportGen.download_json(filename, headers, *data)
    elif data_type == "xls":
        generated = ReportGen.download_xls(filename, headers, *data)
    elif data_type == "html":
        generated = ReportGen.download_html(filename, headers, *data)
    else:
        return False

    report_description = "Users Report for the period between %s and %s" % (str(start.date()), str(end.date()))
    if not just_data:
        res = create_report(handle, filename, data_type, report_description, start, end, client_id=client_id)
    return generated


@celery.task(queue="reports")
def customers_report(client_id, start, data_type, just_data, period=1):
    """ Generates a report listing client's customers within the specified period """

    handle = "client_customers"

    end = start + timedelta(days=period)
    book_data = ()
    query = db.session.query(models.Customer.client_id, func.count(models.Customer.id)).filter(
        models.Customer.date_created >= start, models.Customer.date_created < end).group_by(
        models.Customer.client_id).all()
    customers_count = []
    for client_id, count in query:
        customers_count.append(count)

    headers = ["date | time", "name", "email", "phone", "city", "state", "country"]

    # create the workbook
    filename = "%s-%s-to-%s" % ("%s-report"%handle, str(start.date()), str(end.date()))

    # Next write the content. Starting from the second row
    for client_id, count in query:
        client = models.Client.query.get(client_id)
        data = []
        for obj in client.customers:
            item = (
                obj.date_created.strftime("%d-%m-%Y | %H:%M"),
                obj.address.full_name,
                obj.address.email,
                obj.address.phone,
                obj.address.city.name,
                obj.address.state.name,
                obj.address.country.name
            )
        data.append(item)

        obj_data = ReportGen.prepare_data(headers, *data)
        book_data = book_data + obj_data

    generated = ReportGen.download_workbook(filename, book_data)

    report_description = "Customers Report for the period between %s and %s" % (
        str(start.date()), str(end.date()))
    res = create_report(handle, filename, data_type, report_description, start, end, client_id=client_id)

    return generated


@celery.task(queue="reports")
def stations_report(client_id, start, data_type, just_data, period=1):
    """ Generates a report listing client's customers within the specified period """
    handle = "client_stations"
    end = start + timedelta(days=period)
    data = []
    query = db.session.query(models.Station).filter(
        models.Station.client_id == client_id, models.Station.date_created >= start,
        models.Station.date_created < end).all()
    headers = ["date created", "name", "manager", "is_active", "city", "state"]
    # create the workbook
    filename = "%s-%s-to-%s" % ("%s-report"%handle, str(start.date()), str(end.date()))

    # Next write the content. Starting from the second row
    for obj in query:
        item = (
            obj.date_created.strftime("%d-%m-%Y | %H:%M"),
            obj.name,
            obj.manager.name,
            "Yes" if obj.is_active else "No",
            obj.city.name if obj.city else "None",
            obj.state.name
        )
        data.append(item)

    if data_type == "csv":
        generated = ReportGen.download_csv(filename, headers, *data)
    elif data_type == "xlsx":
        generated = ReportGen.download_xlsx(filename, headers, *data)
    elif data_type == "json":
        generated = ReportGen.download_json(filename, headers, *data)
    elif data_type == "xls":
        generated = ReportGen.download_xls(filename, headers, *data)
    elif data_type == "html":
        generated = ReportGen.download_html(filename, headers, *data)
    else:
        return False
    report_description = "Stations Report for the period between %s and %s" % (str(start.date()), str(end.date()))
    if not just_data:
        res = create_report(handle, filename, data_type, report_description, start, end, client_id=client_id)
    return generated


@celery.task(queue="reports")
def devices_report(client_id, start, data_type, just_data, period=1):
    """ Generates a report listing client's customers within the specified period """

    handle = "client_devices"

    end = start + timedelta(days=period)
    data = []
    query = db.session.query(models.Device).filter(
        models.Device.client_id == client_id, models.Device.date_created >= start,
        models.Device.date_created < end).all()

    headers = ["date created", "code", "reference_code", "is_active"]

    # create the workbook
    filename = "%s-%s-to-%s" % ("%s-report"%handle, str(start.date()), str(end.date()))

    # Next write the content. Starting from the second row
    for obj in query:
        item = (
            obj.date_created.strftime("%d-%m-%Y | %H:%M"),
            obj.code,
            obj.reference_code,
            "Yes" if obj.is_active else "No"
        )
        data.append(item)

    if data_type == "csv":
        generated = ReportGen.download_csv(filename, headers, *data)
    elif data_type == "xlsx":
        generated = ReportGen.download_xlsx(filename, headers, *data)
    elif data_type == "json":
        generated = ReportGen.download_json(filename, headers, *data)
    elif data_type == "xls":
        generated = ReportGen.download_xls(filename, headers, *data)
    elif data_type == "html":
        generated = ReportGen.download_html(filename, headers, *data)
    else:
        return False

    report_description = "Devices Report for the period between %s and %s" % (str(start.date()), str(end.date()))
    if not just_data:
        res = create_report(handle, filename, data_type, report_description, start, end, client_id=client_id)
    return generated

