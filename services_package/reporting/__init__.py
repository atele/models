from flask import current_app as app
from services_package.basic import ReportService
from model_package.signals import *
from services_package.storage import uploader
from pprint import pprint

logger = app.logger
celery = app.celery

ReportGen = app.reporting.generate
# class ReportGen:
#     def __init__(self):
#         pass
#
#     # For multiple worksheets pass the datasets to DataBook like tablib.Databook((dataset1, dataset2))
#     @classmethod
#     def prepare_data(cls, headers, *raw_data):
#         if len(raw_data) > 0 and type(raw_data[0]) is dict:
#             data = tablib.Dataset()
#             data.dict = raw_data
#         elif len(raw_data) > 0 and type(raw_data[0]) is tuple:
#             data = tablib.Dataset(*raw_data, headers=headers)
#         else:
#             data = tablib.Dataset()
#
#         return data
#
#     @classmethod
#     def download_csv(cls, filename, headers, *data):
#
#         data = cls.prepare_data(headers, *data)
#         fullpath = "%s/%s.csv" % (app.config.get("REPORTS_DIR"), filename)
#         with open(fullpath, 'wb') as f:
#             f.write(data.csv)
#
#         return data.csv
#
#     @classmethod
#     def download_html(cls, filename, headers, *data):
#         data = cls.prepare_data(headers, *data)
#         return data.html
#
#     @classmethod
#     def download_json(cls, filename, headers, *data):
#         data = cls.prepare_data(headers, *data)
#         fullpath = "%s/%s" % (app.config.get("REPORTS_DIR"), "report-generation.json")
#         with open(fullpath, 'wb') as f:
#             f.write(data.json)
#         return data.json
#
#     @classmethod
#     def download_xlsx(cls, filename, headers, *data):
#         data = cls.prepare_data(headers, *data)
#
#         fullpath = "%s/%s.xlsx" % (app.config.get("REPORTS_DIR"), filename)
#         with open(fullpath, 'wb') as f:
#             f.write(data.xlsx)
#
#         return data.xlsx
#
#     @classmethod
#     def download_xls(cls, filename, headers, *data):
#         data = cls.prepare_data(headers, *data)
#
#         fullpath = "%s/%s.xls" % (app.config.get("REPORTS_DIR"), filename)
#         with open(fullpath, 'wb') as f:
#             f.write(data.xls)
#
#         return data.xls
#
#     @classmethod
#     def download_workbook(cls, filename, data):
#         book = tablib.Databook(data)
#         fullpath = "%s/%s.xlsx" % (app.config.get("REPORTS_DIR"), filename)
#         with open(fullpath, 'wb') as f:
#             f.write(book.xlsx)
#
#         return book.xlsx


@celery.task(queue="reports")
def create_report(handle, filename, f_ext, description, start, end, client_id=None):
    """ Creates, uploads and stores the report object by uploading the file to storage and recording it"""
    data = {"client_id":client_id,"handle": handle, "name": filename, "description": description, "start": start, "end": end}
    report = ReportService.create(**data)
    full_path = "%s/%s.%s" % (app.config.get("REPORTS_DIR"), filename, f_ext)
    try:
        # res = private_upload_to_cloudfiles(report.handle, fullpath)
        filename = "%s.%s" % (filename, f_ext)
        container = "reports-%s"%handle
        res = uploader.upload_file(filename, full_path, container)
        data = {"file_path": res}
        report = ReportService.update(report.id, **data)
        report_generated.send(report.id, filepath=full_path)
        return report
    except:
        raise
