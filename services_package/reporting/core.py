from dateutil.parser import parse
from flask import current_app as app
from services_package.reporting import client, admin, customer


celery = app.celery
logger = app.logger


@celery.task(queue="reports")
def custom_report(start, end, handle, data_type, just_data=False):
    """ Generates a canned report for a specified period """
    report_types = {
        "clients": admin.clients_report,
        "users": admin.users_report,
        "customers": admin.customers_report
    }

    _func = report_types.get(handle, None)

    if _func:
        print start, end
        start = parse(start) if isinstance(start, (str, unicode)) else start
        end = parse(end) if isinstance(end, (str, unicode)) else end

        period = (end - start).days

        return _func(start, data_type, just_data, period)


@celery.task(queue="reports")
def client_custom_report(client_id, start, end, handle, data_type, just_data=False):
    """ Generates a canned report for a specified period """
    report_types = {
        "stations": client.stations_report,
        "devices": client.devices_report
    }

    _func = report_types.get(handle, None)

    if _func:
        print start, end
        start = parse(start) if isinstance(start, (str, unicode)) else start
        end = parse(end) if isinstance(end, (str, unicode)) else end

        period = (end - start).days

        return _func(client_id, start, data_type, just_data, period)


@celery.task(queue="reports")
def customer_custom_report(customer_id, start, end, handle, data_type, just_data=False):
    """ Generates a canned report for a specified period """
    report_types = {
        "devices": customer.devices_report
    }

    _func = report_types.get(handle, None)

    if _func:
        print start, end
        start = parse(start) if isinstance(start, (str, unicode)) else start
        end = parse(end) if isinstance(end, (str, unicode)) else end

        period = (end - start).days

        return _func(customer_id, start, data_type, just_data, period)


