"""
printing.py

This module handles all pdf document generation.

@Author: Olukunle Ogunmokun
@Date: October 22, 2015

"""

import os
from flask import render_template, current_app as app
from flask_weasyprint import HTML, render_pdf
from weasyprint import HTML as _HTML


def pdf_response(html_string):
    """ converts the html string into a pdf document and returns a HTTP response for it """

    return render_pdf(HTML(string=html_string))


def pdf_stream(html_string):
    """ converts the html string into a pdf stream. Will be used along with a file structure """
    return HTML(string=html_string).write_pdf()


def pdf_file(html_string, filename):
    """ converts the html string into a pdf temporary file and returns the source. Useful for email attachements"""

    if not filename.endswith(".pdf"):
        filename += ".pdf"

    _filename = os.path.join(app.config.get("UPLOADS_DIR"), "pdf/%s" % filename)

    _doc = open(_filename, "wb")

    _pdf = _HTML(string=html_string).write_pdf()

    _doc.write(_pdf)

    _doc.close()

    os.chdir(os.path.join(app.config.get("UPLOADS_DIR"), "pdf"))

    filepath = os.path.join(os.path.basename(os.getcwd()), filename)

    return filepath


def create_pdf(template_path, filename, **data):
    """ create a temporary pdf file from an html template """

    html = render_template(template_path, **data)

    _pdf = pdf_file(html, filename)

    return _pdf


def return_pdf(template_path, **data):
    """ create a temporary pdf file from an html template """

    html = render_template(template_path, **data)

    _pdf = pdf_response(html)

    return _pdf
