import pprint
from datetime import datetime

from flask import current_app as app
from utilities import utils, exceptions
import basic

from integrations_package import pycard, ServiceLabs
from model_package import models


with app.app_context():

    db = models.app.db
    logger = app.logger

    CustomerBillingService = ServiceLabs.create_instance(models.CustomerBilling, db)
    CustomerBillingRecordService = ServiceLabs.create_instance(models.CustomerBillingRecord, db)
    BaseBillingService = ServiceLabs.create_instance(models.Billing, db)
    BillingRecordService = ServiceLabs.create_instance(models.BillingRecord, db)
    BaseInvoiceService = ServiceLabs.create_instance(models.Invoice, db)
    BaseSubscriptionService = ServiceLabs.create_instance(models.Subscription, db)
    BaseCardService = ServiceLabs.create_instance(models.Card, db)
    BankAccountService = ServiceLabs.create_instance(models.BankAccount, db)
    WalletService = ServiceLabs.create_instance(models.Wallet, db)

    class BillingService(BaseBillingService):
        """
        Custom modification to base billing service
        """

        @classmethod
        def create(cls, ignored=None, **kwargs):
            """
            custom create method
            :param ignored:
            :param kwargs:
            :return: user model object
            """
            if not ignored:
                ignored = ["id", "date_created", "last_updated"]

            data = utils.clean_kwargs(ignored, kwargs)
            today=datetime.today()
            month=today.strftime("%B")
            year="%s"%today.year

            client_id = data.get('client_id',0)
            client = models.Client.query.get(client_id)
            invoice = models.Invoice.filter_by(client_id=client_id, month=month,year=year).first()

            if not invoice:
                _data={"month":month,"year":year,"client_id":client_id,"service_charge":client.subscription_fee}
                invoice = InvoiceService.create(ignored, **_data)

            if invoice and invoice.is_paid==True:
                _data={"month":month,"year":year,"client_id":client_id, "service_charge":0}
                invoice = InvoiceService.create(ignored, **_data)

            data["invoice_id"]=invoice.id
            billing = BaseBillingService.create(ignored, **data)

            return billing

    class InvoiceService(BaseInvoiceService):
        """
        Custom modification to base invoice service
        """

        @classmethod
        def create(cls, ignored=None, **kwargs):
            """
            custom create method
            :param ignored:
            :param kwargs:
            :return: user model object
            """
            if not ignored:
                ignored = ["id", "date_created", "last_updated"]

            data = utils.clean_kwargs(ignored, kwargs)
            code =utils.code_generator()
            if models.Invoice.query.filter(models.Invoice.code==code).first():
                code = utils.numeric_code_generator()
            data["code"]=code
            invoice = BaseInvoiceService.create(ignored, **data)

            return invoice

    class SubscriptionService(BaseSubscriptionService):
        """
        Custom modification to base subscription service
        """

        @classmethod
        def create(cls, ignored=None, **kwargs):
            """
            custom create method
            :param ignored:
            :param kwargs:
            :return: user model object
            """
            if not ignored:
                ignored = ["id", "date_created", "last_updated"]

            data = utils.clean_kwargs(ignored, kwargs)

            client = models.Client.query.get(data['client_id'])
            # client = account.ClientService.get(data['client_id'])
            service = basic.DefaultService.get(data['service_id'])

            subscription = cls.filter_by(client_id=client.id, service_id=service.id)

            if not subscription:
                subscription = BaseSubscriptionService.create(ignored, **data)

            return subscription


    class CardService(BaseCardService):
        """"""

        @classmethod
        def charge(cls, card_id, **kwargs):
            """
            method used for a payment by a saved card
            :param card_id:
            :param kwargs:
            :return:
            """

            card = cls.get(card_id)

            if not card:
                raise exceptions.ObjectNotFoundException(models.RecurrentCard, card_id)

            return FlutterCardService.charge(card.id, **kwargs)

        @classmethod
        def pay(cls, **kwargs):
            """
            method used for a payment by an unsaved card
            :param kwargs:
            :return:
            """

            return FlutterCardService.pay(**kwargs)

        @classmethod
        def validate(cls, code, **kwargs):
            """
            validate card
            :param code:
            :param kwargs:
            :return:
            """
            return FlutterCardService.validate(code, **kwargs)

        @classmethod
        def check_card(cls, **kwargs):
            """
            Check card
            :param kwargs:
            :return:
            """

            return FlutterCardService.check_card(**kwargs)

        @classmethod
        def free_register(cls, **kwargs):
            """
            register card
            :param kwargs:
            :return:
            """
            return FlutterCardService.free_register(**kwargs)

        @classmethod
        def register(cls, **kwargs):
            """
            register card
            :param kwargs:
            :return:
            """
            return FlutterCardService.register(**kwargs)


    class FlutterCardService(object):
        """
        Flutterwave implementation for handling card payments
        """

        @classmethod
        def charge(cls, card_id, **kwargs):
            """method used for a payment by a saved card"""

            card = CardService.get(card_id)

            pprint.pprint("in fluttercradservice")
            pprint.pprint(kwargs)
            if not card:
                raise exceptions.ObjectNotFoundException(models.RecurrentCard, card_id)

            kwargs['chargetoken'] = card.token
            kwargs['currency'] = "NGN"
            kwargs['custid'] = "ijektngkdl"
            print '=======kwargs==============='
            pprint.pprint(kwargs)
            # trans = TransactionService.create(status_code='pending', type_="debit", mode='card', **kwargs)

            res = app.flutterwave.card.charge(**kwargs)

            data = res.get('data')
            status = res.get('status')
            res_code = data.get('responsecode', None)

            # status_code = trans.status_code

            if status == "success" and res_code in ["0", "00"]:
                kwargs['response_status'] = status
                kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
                kwargs['response_code'] = res_code
                kwargs['transaction_reference'] = data.get('transactionreference', None)
                kwargs['status'] = 'successful'
            # trans = TransactionService.update(trans.id, status_code=status_code, **kwargs)
            # return trans
            else:
                kwargs['response_status'] = status
                kwargs['response_message'] = res.get('responsemessage', None)  # get response message from res
                kwargs['response_code'] = res_code
                kwargs['status'] = 'failed'
                raise Exception
            # notifier.send()  # to.do: send out a notification here............

            # trans = TransactionService.update(trans.id, status_code=status_code, **kwargs)

            # return trans
            return kwargs

        @classmethod
        def pay(cls, **kwargs):
            """method used for a payment by an unsaved card"""

            app.logger.info('before we start')
            pprint.pprint(kwargs)

            card_data = cls.check_card(**kwargs)
            brand = card_data.get('brand')
            cvv = card_data.get('cvv')
            pin = kwargs.get("pin", '1234')
            # pin = kwargs.get("pin", None)

            kwargs.update(card_data)

            authmodel = app.flutterwave.AUTHMODEL.choice(brand)
            validateoption = app.flutterwave.VALIDATEOPTION.SMS

            kwargs['code'] = utils.code_generator()
            kwargs['method'] = "card"
            kwargs['mask'] = card_data.get('mask')
            kwargs['expiryyear'] = card_data.get('expiryyear')
            kwargs['exp_year'] = card_data.get('expiryyear')
            kwargs['expirymonth'] = card_data.get('expirymonth')
            kwargs['exp_month'] = card_data.get('expirymonth')
            kwargs['brand'] = brand
            kwargs['cvv'] = str(cvv)
            kwargs['pin'] = str(pin)
            # kwargs['pin'] = pin
            kwargs['currency'] = "NGN"
            kwargs['authmodel'] = authmodel
            kwargs['validateoption'] = validateoption

            # kwargs['narration'] = kwargs.get('narration', "payment for product")
            kwargs['customerId'] = 'ad4578'
            kwargs['custid'] = 'ad4578'

            pprint.pprint("Just before creating transaction")
            pprint.pprint(card_data)

            kwargs['authmodel'] = authmodel

            trans = TransactionService.get(kwargs.get("trans_id"))

            if authmodel == app.flutterwave.AUTHMODEL.VBVSECURECODE:
                ""
                kwargs['responseurl'] = trans.response_url

            # check that the pin is required. This action will happen only when the authmodel is not vbvsecurecode
            # if authmodel != flutterwave.AUTHMODEL.VBVSECURECODE and not pin:
            #     # take in the pin number and start again
            #     raise FurtherActionException(kwargs)

            pprint.pprint("card data for FLW")
            pprint.pprint(kwargs)
            res = app.flutterwave.card.pay(**kwargs)
            print "===res==="
            pprint.pprint(res)

            data = res.get('data')
            status = res.get('status')
            res_code = data.get('responsecode', None)

            if status == "success" and res_code in ["02"]:
                kwargs['response_status'] = status
                kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
                kwargs['otptransactionidentifier'] = data.get('otptransactionidentifier')
                kwargs['response_code'] = res_code
                kwargs['transaction_reference'] = data.get('transactionreference', None)
                # trans = TransactionService.update(trans.id, status_code='validation_required', **kwargs)
                # data["code"] = trans.code
                raise exceptions.FurtherActionException(data)

            elif status == "success" and res_code in ["00", "0"]:
                kwargs['response_status'] = status
                kwargs['status'] = "successful"
                kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
                kwargs['token'] = data.get('responsetoken', None)
                kwargs['response_code'] = res_code
                kwargs['transaction_reference'] = data.get('transactionreference', None)

            else:
                kwargs['response_status'] = status
                kwargs['status'] = "failed"
                kwargs['response_message'] = data.get('responsemessage', None)
                kwargs['response_code'] = res_code
                kwargs['transaction_reference'] = data.get('transactionreference', None)
                raise exceptions.ValidationFailed({"cardno": ["Invalid or expired card"]})

            # return trans
            return kwargs

        @classmethod
        def validate(cls, code, **kwargs):

            # trans = Transaction.query.filter(Transaction.code == str(code.upper())).first()

            # if not trans:
            #     raise ObjectNotFoundException(Transaction, code)
            #
            # kwargs['trxreference'] = trans.transactionreference
            # kwargs['otptransactionidentifier'] = trans.otptransactionidentifier
            # kwargs['currency'] = trans.currency
            # kwargs['amount'] = trans.amount

            res = app.flutterwave.card.validate(**kwargs)

            data = res.get('data')
            status = res.get('status')
            res_code = data.get('responsecode', None)

            # status_code = trans.status_code
            status_code = ""
            # request_obj = Request.query.filter_by(request_key=trans.request_key).first()

            if status == "success" and res_code in ["0", "00", "200"]:
                kwargs['response_status'] = status
                kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
                kwargs['response_code'] = res_code
                kwargs['token'] = data.get("responsetoken", None)
                status_code = 'successful'
            else:
                kwargs['response_status'] = status
                kwargs['response_message'] = res.get('responsemessage', None)  # get response message from res
                kwargs['response_code'] = res_code
                status_code = 'failed'

                # trans = TransactionService.update(trans.id, status_code=status_code, **kwargs)
                # trans_data = trans.level_dict()
                # trans_data['merchant_key'] = request_obj.merchant_key

                # if trans.status_code == 'successful':
                #     send_payment_notification.delay(request_obj.id, 'pc', 'Payment Successful', **trans_data)
                # else:
                #     send_payment_notification.delay(request_obj.id, 'pf', 'Payment Failed', **trans_data)

                # return trans

        @classmethod
        def check_card(cls, **kwargs):
            """ Validate the card and return the required values for storage """

            cardno = kwargs.get("card_number")
            expiry_year = int(kwargs.get("expiry_year"))
            expiry_month = int(kwargs.get("expiry_month"))
            cvv = kwargs.get("cvv")
            pin = kwargs.get("pin")

            # clean the parameters of the card
            # expiry_month, expiry_year = expiry.split("/")
            # expiry_month = int(expiry_month)
            # expiry_year = int(expiry_year)
            cvv = cvv

            card = pycard.Card(number=cardno, month=expiry_month, year=expiry_year, cvc=cvv)

            if card.is_valid and not card.is_expired:
                return dict(cardno=card.number, expirymonth=card.exp_date.mm, expiryyear=card.exp_date.yy,
                            friendly_brand=card.friendly_brand,
                            cvv=card.cvc, brand=card.brand, mask=card.mask, is_expired=card.is_expired, pin=pin)
            else:
                raise exceptions.ValidationFailed({"cardno": ["Invalid card supplied"]})

        @classmethod
        def free_register(cls, **kwargs):
            pprint.pprint(kwargs)
            card_epiry = kwargs.get("card_expiry", "").replace(" ", "").split("/")
            kwargs["card_number"] = kwargs.get("card_number", "").replace(" ", "")
            kwargs["expiry_month"] = card_epiry[0] or ""
            kwargs["expiry_year"] = card_epiry[1] or ""
            card_data = cls.check_card(**kwargs)
            # request_key = kwargs.get('request_key')

            print card_data, "=====card_data======"

            authmodel = app.flutterwave.AUTHMODEL.NOAUTH
            kwargs['authmodel'] = authmodel
            kwargs['currency'] = app.flutterwave.CURRENCYMODEL.NGN
            kwargs['validateoption'] = app.flutterwave.VALIDATEOPTION.SMS

            kwargs.update(card_data)

            res = app.flutterwave.card.tokenize(**kwargs)
            print res, "=====tokenize======="

            data = res.get('data')
            status = res.get('status')
            res_code = data.get('responsecode', None)

            if status == "success" and res_code in ["0", "00", "200"]:

                kwargs['token'] = data.get('responsetoken')
                kwargs['exp_month'] = kwargs.get('expirymonth', None)
                kwargs['exp_year'] = kwargs.get('expiryyear', None)
                kwargs['mask'] = card_data.get('mask')
                kwargs['brand'] = card_data.get('brand')
                pprint.pprint("kwargs for address======================%s" % kwargs)
                address = basic.AddressService.filter_by(email=kwargs.get('email', ''))
                if not address:
                    address = basic.AddressService.create(**kwargs)
                kwargs["address_id"] = address.id
                card = CardService.create(**kwargs)
                return card_data

            else:
                kwargs['response_status'] = status
                kwargs['response_message'] = res.get('responsemessage', None)  # get response message from res
                kwargs['response_code'] = res_code

                raise exceptions.ValidationFailed({"cardno": ["Invalid or expired card"]})

        @classmethod
        def register(cls, **kwargs):
            card_epiry = kwargs.get("card_expiry", "").replace(" ", "").split("/")
            kwargs["card_number"] = kwargs.get("card_number", "").replace(" ", "")
            kwargs["expiry_month"] = card_epiry[0] or ""
            kwargs["expiry_year"] = card_epiry[1] or ""

            card_data = cls.check_card(**kwargs)
            # request_key = kwargs.get('request_key')

            authmodel = app.flutterwave.AUTHMODEL.BVN

            kwargs['authmodel'] = authmodel
            kwargs['currency'] = app.flutterwave.CURRENCYMODEL.NGN
            kwargs['validateoption'] = app.flutterwave.VALIDATEOPTION.SMS
            kwargs['narration'] = "Atele Random Debit"
            kwargs.update(card_data)

            res = app.flutterwave.card.tokenize(**kwargs)

            data = res.get('data')
            status = res.get('status')
            res_code = data.get('responsecode', None)
            if status == "success" and res_code in ["0", "00", "200"]:

                kwargs['token'] = data.get('responsetoken')
                kwargs['exp_month'] = kwargs.get('expirymonth', None)
                kwargs['exp_year'] = kwargs.get('expiryyear', None)
                kwargs['mask'] = card_data.get('mask')
                kwargs['brand'] = card_data.get('brand')
                pprint.pprint("kwargs for address======================%s" % kwargs)
                address = basic.AddressService.filter_by(email=kwargs.get('email', ''))
                if not address:
                    address = basic.AddressService.create(**kwargs)
                kwargs["address_id"] = address.id
                card = CardService.create(**kwargs)
                data["card_id"]=card.id
                return status, data
            elif status == "success" and res_code in ["RR"]:
                return status, data
            else:
                kwargs['response_status'] = status
                kwargs['response_message'] = res.get('responsemessage', None)  # get response message from res
                kwargs['response_code'] = res_code

                raise exceptions.ValidationFailed({"cardno": ["Invalid or expired card"]})


    class FlutterBankAccountService(object):
        """
            Flutterwave implementation for handling bank account payments
        """

        @classmethod
        def verify_account(cls, **kwargs):
            """method used for a payment by bank account"""

            bank_id = kwargs.get("bank_id", 0)
            bank = basic.BankService.get(bank_id)

            kwargs["recipientaccount"] = kwargs.get("account_number", "")
            kwargs["destbankcode"] = bank.code
            res = app.flutterwave.bank.account_enquiry(**kwargs)

            return res

        @classmethod
        def register(cls, **kwargs):
            """method used for registering a bank account"""

            bank_id = kwargs.get("bank_id", 0)
            bank = basic.BankService.get(bank_id)

            kwargs["recipientaccount"] = kwargs.get("account_number", "")
            kwargs["destbankcode"] = bank.code

            try:
                res = app.flutterwave.bank.account_enquiry(**kwargs)
                if res.get("status","success"):
                    obj = basic.BankAccountService.create(**kwargs)
                    return obj

            except Exception, e:
                return cls.error_response(e)