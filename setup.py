from setuptools import find_packages, setup

setup(name='model_package',
      version='0.1',
      description='Model Packages',
      url='https://github.com/stikks/Utilities',
      author='stikks',
      author_email='styccs@gmail.com',
      include_package_data=True,
      packages=find_packages(),
      install_requires=[
          'requests',
          'flask',
          'flask-sqlalchemy',
          'flask-wtf',
          'flask-weasyprint',
          'flask-principal',
          'flask-restful',
          'PyJWT==1.5.0',
          'psycopg2',
          'shapely',
          'geoalchemy2',
          'flask-login',
          'flask-bcrypt',
          'blinker',
          'messagerie',
          'celery',
          'paho-mqtt'
      ],
      dependency_links=[
          'git+https://github.com/stikks/Utilities.git']
      )