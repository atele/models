import json

from flask import current_app as app
from pymongo import MongoClient


class MongoService(object):

    __client__ = MongoClient()
    __db__ = __client__[app.config['MONGODB_DATABASE']]

    @classmethod
    def record_activity(cls, identifier, actor, verb, _object, object_type, browser, device, device_type, os, timestamp):
        """
        record activity into mongodb
        :return:
        """
        try:
            activities = cls.__db__.activities
            record = {
                'id': identifier,
                'actor': actor,
                'verb': verb,
                'object': _object,
                'object_type': object_type,
                'browser': browser,
                'device': device,
                'device_type': device_type,
                'os': os,
                'timestamp': timestamp
            }
            activities.insert_one(record)
            return True
        except Exception, e:
            app.log.error('[ACTOR: %s][VERB: %s][ACTION: %s][STATUS: FAILED]' % (actor, verb, _object))
            app.log.error(e)
            return False

    @classmethod
    def get_next_sequence(cls, collection_name):
        return cls.__db__[collection_name].find().count()+1

    @classmethod
    def record_data(cls, collection_name, identifier, timestamp, **kwargs):
        """
        record transaction
        :param collection_name
        :param identifier:
        :param timestamp:
        :param kwargs:
        :return:
        """
        try:
            records = cls.__db__.get_collection(collection_name)
            record = {
                'id': identifier,
                'timestamp': timestamp
            }
            record.update(kwargs)
            records.insert_one(record)
            return True
        except Exception, e:
            app.logger.error('[IDENTIFIER: %s][TIMESTAMP: %s][STATUS: FAILED]' % (identifier, timestamp))
            app.logger.error(e)
            return False

    @classmethod
    def update_record(cls, collection_name, identifier, **kwargs):
        """
        update mongo record
        :param collection_name
        :param identifier
        :return
        """
        try:
            records = cls.__db__.get_collection(collection_name)
            return records.update_one({"id": identifier}, {
                '$set': kwargs
            })
        except Exception, e:
            app.logger.error('[IDENTIFIER: %s][ACTION: UPDATE][STATUS: FAILED][BODY: %s]' % (identifier, json.dumps(kwargs)))
            app.logger.error(e)
            return False

    @classmethod
    def delete_record(cls, collection_name, identifier):
        """
        delete mongo record
        :param collection_name
        :param identifier
        :return
        """
        try:
            records = cls.__db__.get_collection(collection_name)
            return records.delete_many({"id": identifier})
        except Exception, e:
            app.logger.error('[IDENTIFIER: %s][ACTION: DELETE][STATUS: FAILED]' % identifier)
            app.logger.error(e)
            return False

    @classmethod
    def query_collection(cls, collection_name, first_only=False, page=1,size=20, **kwargs):
        """
        query notifications based on data
        :param collection_name
        :param kwargs
        :return
        """
        try:
            data = cls.__db__[collection_name].find(kwargs)

	    if first_only:
                return next((i for i in data))

	    response = [i for i in data]
            return cls.build_pagination(response, page=page, size=size)
        except Exception, e:
            raise e

    @classmethod
    def build_pagination(cls, list_objs, page=1, size=20):
        from operator import itemgetter

        total = len(list_objs)
        pages = (total / size) + min(1, total % size)
        prev = max(1, page - 1) if page > 1 else None
        next = min(pages, page + 1) if page < pages else None

        new_list = sorted(list_objs, key=itemgetter('id'))

        page_ind = page - 1

        start = page_ind * size
        end = start + size

        items_list = new_list[start:end] if page <= pages else []

        return {"page": page, "items": items_list, "total": total, "size": size, "prev": prev, "next": next,
                "pages": pages}


