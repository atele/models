import logging
from influxdb import InfluxDBClient

from utilities import ServiceLabs

logger = ServiceLabs.setup_log('mqtt_info', '/var/log/tsdb/info.log')
error_logger = ServiceLabs.setup_log('mqtt_error', '/var/log/tsdb/error.log', logging.ERROR)


class DataStorage(object):
    __client = InfluxDBClient('localhost', 8086, 'root', 'root', 'atele')
    # __client = InfluxDBClient(host='localhost', use_udp=True, udp_port=4444, username='root', password='root',
    #                           database='atele')

    @classmethod
    def create_database(cls, host, port, username, password, database_name):
        """
        create influxDB database
        :return:
        """
        try:
            client = InfluxDBClient(host=host, port=port, username=username,
                                    password=password)
            client.create_database(database_name)
            client.create_retention_policy(name='default_policy', duration='INF', database=database_name,
                                           replication='3', default=True)
            logger.info(
                '[OBJECT: TimeSeriesDatabase][ACTOR: {}][[ACTION: DATABASE CREATION SUCCESSFUL]'.format(database_name))
            return True
        except Exception as e:
            error_logger.error('[OBJECT: TimeSeriesDatabase][ACTOR: {}][[ACTION: DATABASE CREATION FAILED]'.format(
                database_name))
            print(e)
            return False

    @classmethod
    def store_data(cls, device_id, tags=dict(), **kwargs):
        """
        store data in influxDB
        :param device_id:
        :param kwargs:
        :return:
        """
        try:
            json_tags = dict(id=device_id)
            json_tags.update(tags)
            json_body = [
                {
                    "measurement": "readings",
                    "tags": json_tags,
                    "fields": kwargs
                }
            ]
            result = cls.__client.write_points(json_body)
            logger.info('[OBJECT: DATA][ACTOR: {}][STATUS:SUCCESS][[DATA: {}]'.format(device_id, kwargs))
            return result
        except Exception as e:
            error_logger.error('[OBJECT: DATA][ACTOR: {}][STATUS:FAILED][[DATA: {}]'.format(device_id, kwargs))
            print(e)
            return False

    @classmethod
    def query_range(cls, start, end, value=None, measurement="readings", key='id'):
        """
        query data from influxDB database
        :param measurement:
        :param key:
        :param now
        :return:
        """
        if value:
            query_string = "select * from {} WHERE {} = '{}' AND timestamp >= {} AND timestamp <= {}".format(measurement,
                                                                               key, value, start, end)
        else:
            query_string = "select * from {} WHERE timestamp >= {} AND timestamp <= {}".format(measurement, start, end)

        result = cls.__client.query(query_string)
        return list(result.get_points(measurement='%s' % measurement))

    @classmethod
    def query_data(cls, value=None, measurement="readings", key='id'):
        """
        query data from influxDB database
        :param measurement:
        :param key:
        :param now
        :return:
        """
        if value:
            query_string = "select * from {} WHERE {} = '{}' order by time desc;".format(measurement, key, value)
        else:
            query_string = 'select * from "%s" order by time desc;' % measurement

        result = cls.__client.query(query_string)
        return list(result.get_points(measurement='%s' % measurement))

    @classmethod
    def query_by_value(cls, value, measurement="readings", start=None, end=None, key='id', tags=dict()):
        """
        query by tag
        :param key:
        :param value:
        :param tag_key:
        :param tag_value:
        :return:
        """
        if start is not None and end is not None:
            query_string = 'select * from "%s" where timestamp >= %s and timestamp <= %s order by time desc;' % (
                measurement, start, end)
        else:
            query_string = "select * from %s where %s='%s' order by time desc;" % (measurement, key, value)
        result = cls.__client.query(query_string)
        return list(result.get_points(measurement='%s' % measurement, tags=tags))
