import xmlrpclib
import odoorpc


class Connector(object):

    def __init__(self, database, host='localhost', port=8069, admin_user='admin', admin_password='admin'):
        self.conn = odoorpc.ODOO(host=host, port=port)
        self.db = self.conn.login(database, admin_user, admin_password)
        self.env = self.conn.env

    @staticmethod
    def change_admin_password(new_password, host='localhost', port=8069, current_password='admin'):
        try:
            odoo = odoorpc.ODOO(host, port=port)
            odoo.db.change_password(current_password, new_password)
            return True
        except Exception as e:
            raise e

    @staticmethod
    def create_database(db_password, database, host='localhost', port=8069, demo=False, admin_password='admin'):
        """
        create odoo database
        on access denied error response, change password
            >>> from integrations.storage.odoo import Connector
            >>> Connector.change_admin_password('new password', host='localhost', port=8069, current_password='admin')
            >>> Connector.create_database('database password', 'database name', 'localhost', 8069, False, 'new password')
        :returns boolean
        """
        try:
            odoo = odoorpc.ODOO(host, port=port)
            odoo.db.create(db_password, database, demo, admin_password=admin_password)
            return True
        except Exception as e:
            raise e

    @staticmethod
    def drop_database(database_name, host='localhost', port=8069, admin_password='admin'):
        try:
            odoo = odoorpc.ODOO(host, port=port)
            odoo.db.drop(admin_password, database_name)
            return True
        except Exception as e:
            raise e

    def query_records(self, model_name, output_fields=('id', 'name'), **kwargs):
        """
        query records matching filter parameters in kwargs
        """
        model = self.env[model_name]
        model.search([(field, '=', value) for field, value in kwargs.iteritems()])


class RpcConnector(object):
    """
    Odoo rpc connector
    """

    def __init__(self, database, username, password, authenticate_endpoint, model_endpoint, host='localhost', port=8069):
        try:
            self.host = host
            self.port = port
            self.address = 'http://{}:{}'.format(self.host, self.port)
            self.database = database
            self.username = username
            self.password = password
            self.authenticate_endpoint = authenticate_endpoint
            self.model_endpoint = model_endpoint
            
            conn = xmlrpclib.ServerProxy('{}{}'.format(self.address, self.authenticate_endpoint))
            self.uid = conn.authenticate(self.database, self.username, self.password, {})
            if self.uid is False:
                raise Exception('Invalid username and password combination')
        except Exception, e:
            raise e

    def query_records(self, model_name, method_name='search_read', **kwargs):
        """
        listing records
        :param model_name:
        :param method_name:
        :param second_method:
        :param kwargs:
            limit - limit the result (integer)
            offset - offset the search request
        :return:
        """
        try:
            models = xmlrpclib.ServerProxy('{}{}'.format(self.address, self.model_endpoint))
            data = dict()

            if kwargs.has_key('limit'):
                data.update({'limit': kwargs.pop('limit')})

            if kwargs.has_key('offset'):
                data.update({'offset': kwargs.pop('offset')})

            filters = list()
            for key, value in kwargs.iteritems():
                filter_ = list('=')
                filter_.insert(0, key)
                filter_.append(value)
                filters.append(filter_)

            result = models.execute_kw(self.database, self.uid, self.password, model_name, method_name,
                                       [filters], data)
            return (item for item in result)
            # if method_name == 'search':
            #     return (item for item in result)
            #
            # return (Payload(**item) for item in result) if not isinstance(result, int) else result
        except Exception, e:
            raise e

    def get_record(self, model_name, obj_id):
        """
        return record matching id
        """
        try:
            data = self.query_records(model_name, **{"id": obj_id})
            return next(data)
        except Exception, e:
            raise Exception('Object matching id - %s not found' % obj_id)

    def records_count(self, model_name, **kwargs):
        """
        :returns integer
        :param model_name:
        :param args:
        :param kwargs:
        :return:
        """
        return self.query_records(model_name, 'search_count', **kwargs)

    def create_record(self, model_name, **kwargs):
        """
        create model object in odoo
        :param model_name:
        :param kwargs:
        :return:
        """
        try:
            models = xmlrpclib.ServerProxy('{}{}'.format(self.address, self.model_endpoint))
            resp = models.execute_kw(self.database, self.uid, self.password, model_name, 'create', [kwargs])
            return resp
        except Exception, e:
            raise e

    def update_record(self, model_name, field, value, **kwargs):
        """
        update odoo model object
        :param model_name:
        :param field:
        :param value:
        :param kwargs:
        :return:
        """
        try:
            object_id = self.query_records(model_name, 'search', **{field: value})
            models = xmlrpclib.ServerProxy('{}{}'.format(self.address, self.model_endpoint))
            return models.execute_kw(self.database, self.uid, self.password, model_name, 'write', [object_id,
                                                                                                             kwargs])
        except Exception, e:
            raise e

    def delete_record(self, model_name, field, value):
        """
        delete odoo model object
        :param model_name:
        :param field:
        :param value:
        :return:
        """
        try:
            object_id = self.query_records(model_name, 'search', **{field: value})
            models = xmlrpclib.ServerProxy('{}{}'.format(self.address, self.model_endpoint))
            return models.execute_kw(self.database, self.uid, self.password, model_name, 'unlink',
                                     [list(object_id)])
        except Exception, e:
            raise e

    def delete_by_id(self, model_name, value):
        """
        delete odoo model object
        :param model_name:
        :param field:
        :param value:
        :return:
        """
        try:
            return self.delete_record(model_name, 'id', value)
        except Exception, e:
            raise e