"""
reporting

"""

from .core import ReportGen

class Reporting:

    def __init__(self, app=None, **kwargs):

        self.app = app

        # Initialize the core engine
        self.generate = ReportGen(app=self.app, **kwargs)
