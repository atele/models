import tablib
import json
from flask import current_app


class ReportMixin(object):
    """ This is the parent class for report data processing. It will contain the core functionality required to
    generate reports in various file formats """

    def __init__(self, app=None, **kwargs):
        self.app = app if app else current_app

        if self.app:
            self.logger = self.app.logger

        self.storage_path = self.app.config.get("REPORTS_DIR")

    # For multiple worksheets pass the datasets to DataBook like tablib.Databook((dataset1, dataset2))
    def prepare_data(self, headers, *raw_data):
        if len(raw_data) > 0 and type(raw_data[0]) is dict:
            data = tablib.Dataset()
            data.dict = raw_data
        elif len(raw_data) > 0 and type(raw_data[0]) is tuple:
            data = tablib.Dataset(*raw_data, headers=headers)
        else:
            data = tablib.Dataset()

        return data


class ReportGen(ReportMixin):
    def download_csv(self, filename, headers, *data):
        data = self.prepare_data(headers, *data)
        fullpath = "%s/%s.csv" % (self.storage_path, filename)
        with open(fullpath, 'wb') as f:
            f.write(data.csv)

        return data.csv

    def download_html(self, filename, headers, *data):
        data = self.prepare_data(headers, *data)
        return data.html

    def download_json(self, filename, headers, *data):
        data = self.prepare_data(headers, *data)
        fullpath = "%s/%s.json" % (self.storage_path, filename)
        with open(fullpath, 'wb') as f:
            f.write(data.json)
        return data.json

    def download_xlsx(self, filename, headers, *data):
        data = self.prepare_data(headers, *data)

        fullpath = "%s/%s.xlsx" % (self.storage_path, filename)
        with open(fullpath, 'wb') as f:
            f.write(data.xlsx)

        return data.xlsx

    def download_xls(self, filename, headers, *data):
        data = self.prepare_data(headers, *data)

        fullpath = "%s/%s.xls" % (self.storage_path, filename)
        with open(fullpath, 'wb') as f:
            f.write(data.xls)

        return data.xls

    def download_workbook(self, filename, data):
        book = tablib.Databook(data)
        fullpath = "%s/%s.xlsx" % (self.storage_path, filename)
        with open(fullpath, 'wb') as f:
            f.write(book.xlsx)

        return book.xlsx

    def prepare_excel_data(self, path):
        imported_data = tablib.Dataset().load(open(path).read())
        print type(imported_data)
        json_string = imported_data.export('json')
        json_data = json.loads(json_string)
        return json_data
