"""
bank.py

@Author: Olukunle Ogunmokun
@Date: June 24, 2017

FlutterwaveMixin bank account apis
"""

from .base import FlutterwaveMixin, NumericStringField, AUTHMODEL, CURRENCYMODEL, COUNTRYMODEL, FLUTTERWAVE_KEY, \
    FLUTTERWAVE_ENDPOINT, \
    FLUTTERWAVE_MERCHANT_ID, VALIDATEOPTION
from marshmallow import Schema, fields, pprint
from marshmallow.validate import Regexp, OneOf

TEST_ACCOUNT_1 = "0690000003"
TEST_ACCOUNT_2 = "0690000004"
TEST_ACCOUNT_3 = "0690000005"


class EnquirySchema(Schema):
    """ equiry account schema for adding bank accounts to flutterwave """

    destbankcode = fields.String(required=True)
    recipientaccount = fields.String(required=True, validate=Regexp(regex="\d+"))


class LinkAccountSchema(Schema):
    """ link account schema for adding bank accounts to flutterwave """

    accountnumber = fields.String(required=True, validate=Regexp(regex="\d+"))
    otptype = fields.String(required=True, validate=OneOf(
        choices=(VALIDATEOPTION.PHONE_OTP, VALIDATEOPTION.ACCOUNT_DEBIT, VALIDATEOPTION.RANDOM_DEBIT)))
    relatedreference = fields.String()
    # trxref = fields.String(required=True)


class ValidateSchema(Schema):
    """ validate bank account information """

    otp = fields.String(required=True)
    relatedreference = fields.String(required=True)
    trxref = fields.String(required=False)
    otptype = fields.String(required=True, validate=OneOf(
        choices=(VALIDATEOPTION.PHONE_OTP, VALIDATEOPTION.ACCOUNT_DEBIT, VALIDATEOPTION.RANDOM_DEBIT)))


class TransferSchema(Schema):
    """ validate transferring funds from a linked account """

    transferamount = NumericStringField(required=True)
    uniquereference = fields.String(required=True)
    destbankcode = fields.String(required=True)
    narration = fields.String(required=True)
    recipientaccount = fields.String(required=True)
    recipientname = fields.String(required=False)
    sendername = fields.String(required=False)
    country = fields.String(required=True)
    currency = fields.String(required=True)
    accounttoken = fields.String(required=True)
    callbackurl = fields.String()  # endpoints to receive feedback from a funds transfer


class Bank(FlutterwaveMixin):
    """ Bank Account api to link with flutterwave payments """

    link_url = "pay/linkaccount"
    validate_url = "pay/linkaccount/validate"
    linked_accounts_url = "pay/getlinkedaccounts"
    send_url = "pay/send"
    enquiry_url = "pay/resolveaccount"
    list_banks_url = "fw/banks"

    def list_banks(self, **kwargs):

        """ Execute a bank account inquiry """

        try:
            # validate the data
            # body = EnquirySchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.list_banks_url, {})

        except Exception, e:

            return self.error_response(e)
            # raise

    def account_enquiry(self, **kwargs):

        """ Execute a bank account inquiry """

        try:
            # validate the data
            body = EnquirySchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.enquiry_url, body.data)

        except Exception, e:

            return self.error_response(e)
            # raise

    def link_account(self, **kwargs):
        """ Link a bank account """

        try:
            # validate the data
            body = LinkAccountSchema(strict=False).load(kwargs)

            # extract the information
            return self.send(self.link_url, body.data)

        except Exception, e:

            return self.error_response(e)
            # raise

    def validate_linked_account(self, **kwargs):
        """ Validate linked account """

        try:
            # validate the data
            body = ValidateSchema(strict=False).load(kwargs)

            # extract the information
            return self.send(self.validate_url, body.data)

        except Exception, e:

            return self.error_response(e)
            # raise

    def transfer_funds(self, **kwargs):
        """ Transfer funds """

        try:
            # validate the data
            body = TransferSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.send_url, body.data)

        except Exception, e:

            # return self.error_response(e)
            raise

    def get_linked_accounts(self, **kwargs):
        """ Validate linked account """

        try:
            # validate the data
            # body = ValidateSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.linked_accounts_url, {})

        except Exception, e:

            return self.error_response(e)
            # raise
