"""
flutterwave.py

@Author: Olukunle, Ogunmokun
@Date: June 24, 2017

This flutterwave consolidation will carry the integrated toolkit for all flutterwave endpoints combined
"""

from .card import Card
from .bank import Bank
from .bvn import Bvn
from .base import FlutterwaveMixin, VALIDATEOPTION, AUTHMODEL, CURRENCYMODEL, COUNTRYMODEL


class Flutterwave(object):
    """ Wrapper class that connects all api methods as child entities of this main Flutterwave parent """

    VALIDATEOPTION = VALIDATEOPTION
    AUTHMODEL = AUTHMODEL
    CURRENCYMODEL = CURRENCYMODEL
    COUNTRYMODEL = COUNTRYMODEL

    def __init__(self, app=None, endpoint=None, key=None, merchant_id=None, **kwargs):
        """ Utilize these parameters to initiate the different and apis """

        self.app = app
        self.endpoint = endpoint
        self.key = key
        self.merchant_id = merchant_id

        # Initialize each of the endpoints
        self.card = Card(app=self.app, endpoint=self.endpoint, key=self.key, merchant_id=self.merchant_id, **kwargs)
        self.bank = Bank(app=self.app, endpoint=self.endpoint, key=self.key, merchant_id=self.merchant_id, **kwargs)
        self.bvn = Bvn(app=self.app, endpoint=self.endpoint, key=self.key, merchant_id=self.merchant_id, **kwargs)
