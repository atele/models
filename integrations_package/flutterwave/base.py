"""
flutterwave.py

@Author: Olukunle Ogunmokun
@Date: June 24, 2017

The flutterwave api integration for payments.
"""

from pprint import pprint

import os
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives import padding
from cryptography.hazmat.backends import default_backend
from flask import current_app
import hashlib
import requests
import base64
import json
import time
from pprint import pprint
from marshmallow import Schema, fields
from marshmallow.validate import Regexp, OneOf

FLUTTERWAVE_ENDPOINT = "http://staging1flutterwave.co:8080/pwc/rest/"
FLUTTERWAVE_KEY = "tk_3jvt5rMpxg1odSeXqmBG"
FLUTTERWAVE_MERCHANT_ID = "tk_I3N9pwQBfe"

class FlutterwaveResponse(dict):
    """ API Response wrapper class that will wrap the information sent back by converting the dict into attributes """

    def __init__(self, **kwargs):
        """ Ingest all attributes on the dictionary and set it as parameters """
        for k, v in kwargs.items():
            setattr(self, k, v)

        # Update the dict with these values also
        self.update(kwargs)


class AUTHMODEL:
    """ Authorization models used """

    PIN = "PIN"
    BVN = "BVN"
    RANDOM_DEBIT = "RANDOM_DEBIT"
    VBVSECURECODE = "VBVSECURECODE"
    NOAUTH = "NOAUTH"

    @classmethod
    def keys(cls):
        """returns all the keys within the model as na array """
        return cls.PIN, cls.BVN, cls.RANDOM_DEBIT, cls.VBVSECURECODE, cls.NOAUTH


    @classmethod
    def choice(cls, brand, authmodel=PIN):

        if brand == "visa":
            authmodel = cls.VBVSECURECODE

        return authmodel

class VALIDATEOPTION:
    """ Validation models used """

    SMS = "SMS"
    VOICE = "VOICE"
    RANDOM_DEBIT = "RANDOM_DEBIT"
    ACCOUNT_DEBIT = "ACCOUNT_DEBIT"
    PHONE_OTP = "PHONE_OTP"

    @classmethod
    def keys(cls):
        "return all keys within the model as an array"
        return (cls.SMS, cls.VOICE, cls.RANDOM_DEBIT, cls.ACCOUNT_DEBIT, cls.PHONE_OTP)

class CURRENCYMODEL:
    """ Currency models used """

    NGN = "NGN" # Nigerian Naira
    USD = "USD" # U.S. Dollars
    GHS = "GHS" # Ghanian Cedies
    GBP = "GBP" # G.B Pounds
    EUR = "EUR" # Euros
    KES = "KES" # Kenyan Shillings

    @classmethod
    def keys(cls):
        "returns all the keys within the model as na array "
        return (cls.NGN, cls.GHS, cls.KES, cls.USD, cls.GBP, cls.EUR)


class COUNTRYMODEL:
    """ Country models used """

    NG = "NG" # Nigerian Naira
    US = "US" # U.S. Dollars
    GH = "GH" # U.S. Dollars
    UK = "UK" # G.B Pounds
    KE = "KE" # Kenyan Shillings

    @classmethod
    def keys(cls):
        "returns all the keys within the model as na array "
        return (cls.NG, cls.GH, cls.KE, cls.US, cls.UK)


class NumericStringField(fields.Field):
    """ Numeric string field to ensure validation and conversion of a numeric value into string """

    # default validator to use
    validator = Regexp(regex="\d+(\.\d+)?")

    def _serialize(self, value, attr, obj):
        return str(value)

    def _deserialize(self, value, attr, obj):
        return str(value)

    def _validate(self, value):
        self.validator(value)


class FlutterwaveMixin(object):
    """ This is the parent class for flutterwave. It will contain the core functionality required to encrypt/decrypt the communication """

    def __init__(self, app=None, endpoint=None, key=None, merchant_id=None, **kwargs):
        self.app = app if app else current_app
        self.endpoint = endpoint if endpoint else self.app.config.get("FLUTTER_WAVE_BASE_URL", endpoint)
        self.key = key if key else self.app.config.get("FLUTTERWAVE_KEY", key)
        self.merchant_id = merchant_id if merchant_id else self.app.config.get("FLUTTERWAVE_MERCHANT_ID", merchant_id)

        if self.app:
            self.logger = self.app.logger

        self.backend = default_backend()
        self.des_key = self.build_3des_key()

        if not self.endpoint.endswith("/"):
            self.endpoint += "/"

        # Begin Header signatures
        _sig = hashlib.sha256()
        _sig.update(self.key)
        _auth_key = _sig.hexdigest()

        _sig.update(str(time.time()))

        nonce = _sig.hexdigest()
        auth = base64.b64encode("%s:%s" % ("atele", _auth_key))
        # End Header signatures

        self.request_headers = {"Content-Type": "application/json", "Authorization": "Basic: %s" % auth, "Nonce": nonce,
                                     "Timestamp": "%s" % time.time(), "SignatureMethod": "sha256", "Signature": _auth_key}

        # self.request_headers = {"Content-Type": "application/json"}

    def build_3des_key(self):
        """ build the key for 3des using md5 digest"""

        m = hashlib.md5()
        m.update(self.key)
        des_key = m.digest()

        return des_key

    def encrypt_3des(self, text):
        """
            Encrypt the specified value using the 3DES symmetric encryption algorithm
            :param text: parameter to encrypt
            :returns cipher_text: 3DES encrypted values
        """

        padder = padding.PKCS7(algorithms.TripleDES.block_size).padder()
        padded_text = padder.update(text) + padder.finalize()

        cipher = Cipher(algorithms.TripleDES(self.des_key), mode=modes.ECB(), backend=self.backend)
        encryptor = cipher.encryptor()
        cipher_text = encryptor.update(padded_text) + encryptor.finalize()

        return cipher_text

    def decrypt_3des(self, cipher_text):
        """
            Decrypt the specified value using the 3DES symmetric decryption algorithm
            :param cipher_text: parameter to decrypt
            :returns u: plain text value
        """

        cipher = Cipher(algorithms.TripleDES(self.des_key), modes.ECB(), backend=self.backend)
        decryptor = cipher.decryptor()
        padded_text = decryptor.update(cipher_text) + decryptor.finalize()

        unpadder = padding.PKCS7(algorithms.TripleDES.block_size).unpadder()
        text = unpadder.update(padded_text) + unpadder.finalize()

        return text

    def build_url(self, stub):
        """ Build a URL by combining the stub with the endpoint"""

        url = "%s%s" % (self.endpoint, stub)
        return url

    def encrypt_data(self, data):
        """ encrypt the data sent in. Will return the 3des version of the dictionary """

        print json.dumps(data, indent=2)

        encrypted_data = dict()

        for k, v in data.items():
            encrypted_data[k] = base64.b64encode(self.encrypt_3des(str(v)))

        return encrypted_data

    def decode_string(self, s):
        """ decode the string if necessary """
        try:
            c = base64.b64decode(s)
            self.logger.info(c)
            return c
        except:
            return s

    def prepare_response(self, response_data):

        pprint(response_data)

        data = response_data["data"]

        for k, v in data.items():
            if k in ['responsehtml', 'responseHtml']:
                response_html = data[k]
                if response_html is not None:
                    decoded_response_html = self.decode_string(response_html)
                    data[k] = self.decrypt_3des(str(decoded_response_html))

        response_data['data'] = data
        # self.logger.info(response_data)

        return response_data

    def send(self, url, data):
        """
        Send the data to the endpoint and return the response.
        The data will be encrypted before posting and the response will be properly handled.
        """
        # 1. Encrypt the data
        encrypted_data = self.encrypt_data(data)

        # 2. Append the merchant_id (unencrypted)
        encrypted_data.update(dict(merchantid=self.merchant_id))
        print "Encrypted Data"
        print json.dumps(encrypted_data, indent=2)

        # 3. Post information by combining the url with the incoming data
        post_url = self.build_url(url)
        response = requests.post(post_url, data=json.dumps(encrypted_data), headers=self.request_headers)

        response_data = self.prepare_response(response.json())
        # response_data = response.json()

        print "Response from %s" % post_url
        print json.dumps(response_data, indent=2)

        try:
            return FlutterwaveResponse(**response_data)
        except:
            return response_data

    def error_response(self, error):
        """ Prepare error response data object """
        _error = dict(status="error", data=error.message)
        print _error
        return FlutterwaveResponse(**_error)
