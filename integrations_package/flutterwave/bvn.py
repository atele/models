"""
compliance.py

@Author: Olukunle, Ogunmokun
@Date: June 24, 2017

FlutterwaveMixin compliance apis
"""

from .base import FlutterwaveMixin, NumericStringField, AUTHMODEL, CURRENCYMODEL, COUNTRYMODEL, FLUTTERWAVE_KEY, FLUTTERWAVE_ENDPOINT, \
    FLUTTERWAVE_MERCHANT_ID,VALIDATEOPTION
from marshmallow import Schema, fields, pprint
from marshmallow.validate import Regexp, OneOf


class BvnSchema(Schema):
    bvn = fields.String(required=True, validate=Regexp(regex="\d+"))
    otpoption = fields.String(required=True, validate=OneOf(choices=VALIDATEOPTION.keys()))
    country = fields.String(required=False, validate=OneOf(choices=COUNTRYMODEL.keys()))


class ResendOTPSchema(Schema):
    transactionreference = fields.String(required=True)
    otpoption = fields.String(required=True, validate=OneOf(choices=VALIDATEOPTION.keys()))
    country = fields.String(required=False, validate=OneOf(choices=COUNTRYMODEL.keys()))

class ValidateBvnSchema(Schema):
    bvn = fields.String(required=True, validate=Regexp(regex="\d+"))
    otp = fields.String(required=True, validate=Regexp(regex="\d+"))
    transactionreference = fields.String(required=True)
    otpoption = fields.String(required=True, validate=OneOf(choices=VALIDATEOPTION.keys()))
    country = fields.String(required=False, validate=OneOf(choices=COUNTRYMODEL.keys()))


class Bvn(FlutterwaveMixin):
    """ BVN verification API """

    verify_url = "bvn/verify"
    resend_url = "bvn/resendotp"
    validate_url = "bvn/validate"

    def verify(self, **kwargs):

        try:
            # validate the data
            body = BvnSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.verify_url, body.data)

        except Exception, e:

            return self.error_response(e)
            # raise

    def resend_otp(self, **kwargs):

        try:
            # validate the data
            body = ResendOTPSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.resend_otp, body.data)

        except Exception, e:

            return self.error_response(e)
            # raise

    def validate(self, **kwargs):

        try:
            # validate the data
            body = BvnOTPSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.resend_otp, body.data)

        except Exception, e:

            return self.error_response(e)
            # raise
