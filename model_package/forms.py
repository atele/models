__author__ = 'stikks & kunsam002'

import re
from datetime import datetime, timedelta
from flask_wtf import FlaskForm
from unicodedata import normalize
from wtforms import Field, SelectField, PasswordField, StringField, FloatField, BooleanField, IntegerField, DateField, \
    SelectMultipleField, RadioField, DateTimeField
from wtforms.validators import DataRequired, Optional, Email, EqualTo, ValidationError
from wtforms.fields.html5 import EmailField
from wtforms import widgets
from model_package import models

_slugify_punct_re = re.compile(r'[\t !"#$%&\'()*\-/<=>?@\[\\\]^_`{|},.]+')


def slugify(text,
            delim=u'-'):
    """
    Generates an ASCII-only slug.

    :param text: The string/text to be slugified
    :param: delim: the separator between words.

    :returns: slugified text
    :rtype: unicode
    """

    result = []
    for word in _slugify_punct_re.split(text.lower()):
        # ensured the unicode(word) because str broke the code
        word = normalize('NFKD', unicode(word)).encode('ascii', 'ignore')
        if word:
            result.append(word)
    return unicode(delim.join(result))


def check_chars(input):
    """ Checks if there's a special character in the text """

    chars = """ '"!@#$%^&*()+=]}-_[{|\':;?/.>,<\r\n\t """
    return any((c in chars) for c in input)


def check_password(input):
    if len(input) < 6:
        return False
    if not re.search(r'\d', input):
        return False
    elif not re.search(r'[A-Z]', input):
        return False
    elif not re.search(r'[a-z]', input):
        return False
    elif not check_chars(input):
        return False
    else:
        return True


class ServiceForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    code = StringField('Code', validators=[DataRequired()])


class LoginForm(FlaskForm):
    class Meta:
        csrf = True
    username = StringField('Username or Email Address', validators=[DataRequired()],
                           description="Please enter a registered username or email")
    password = PasswordField('Password', validators=[DataRequired()], description="Please enter your valid password")


class SignupForm(FlaskForm):
    full_name = StringField('Your Full Name', validators=[DataRequired()])

    password = PasswordField('Password', validators=[DataRequired()])
    verify_password = PasswordField('Verify Password', validators=[DataRequired(), EqualTo('password')])
    terms = BooleanField('Terms and conditions', validators=[DataRequired()])


class ForgotPasswordForm(FlaskForm):
    username = StringField('Username or Email Address', validators=[DataRequired()])


class OnboardingForm(FlaskForm):
    client_name = StringField('Company Name', validators=[DataRequired()])
    client_address = StringField('Address', validators=[DataRequired()])
    client_city_id = SelectField('Client City', coerce=int, validators=[DataRequired()])
    client_state_id = SelectField('Client State', coerce=int, validators=[DataRequired()])
    client_country_id = SelectField('Client Country', coerce=int, validators=[DataRequired()])
    sub_domain = StringField('Sub Domain', validators=[DataRequired()])
    url = StringField('Website Address', validators=[Optional()])

    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired()])
    phone = StringField('Phone Number', validators=[Optional()])
    password = PasswordField('Password', validators=[DataRequired()])
    address = StringField('Address', validators=[Optional()])
    city_id = SelectField('City', coerce=int, validators=[Optional()])
    state_id = SelectField('State', coerce=int, validators=[Optional()])
    country_id = SelectField('Country', coerce=int, validators=[Optional()])

    # subscriptions = StringField('Subscriptions', validators=[DataRequired()])
    subscriptions = SelectMultipleField('Sunscriptions', coerce=int,
                                        description="Select the packages you will like to subscribe to",
                                        validators=[DataRequired()])


class CountryForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    code = StringField('Code', validators=[DataRequired()])


class StateForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    code = StringField('Code', validators=[DataRequired()])
    country_id = SelectField('Country', coerce=int, validators=[DataRequired()])


class CityForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    url = StringField('Url', validators=[Optional()])


class AddressForm(FlaskForm):
    line1 = StringField('Street Address', validators=[DataRequired()])
    line2 = StringField('Landmark')
    phone = StringField('Phone', validators=[DataRequired()])
    first_name = StringField('First Name', validators=[Optional()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    email = EmailField('Email', validators=[Email(), DataRequired()])
    city_id = SelectField('City', coerce=int, validators=[DataRequired()])
    state_id = SelectField('State', coerce=int, validators=[DataRequired()])
    country_id = SelectField('Country', coerce=int, validators=[DataRequired()])
    longitude = StringField('Longitude', validators=[Optional()])
    latitude = StringField('Latitude', validators=[Optional()])


class LocationForm(FlaskForm):
    line1 = StringField('Street Address', validators=[DataRequired()])
    line2 = StringField('Landmark', validators=[Optional()])
    city_id = SelectField('City', coerce=int, validators=[Optional()])
    state_id = SelectField('State', coerce=int, validators=[DataRequired()])
    country_id = SelectField('Country', coerce=int, validators=[DataRequired()])
    longitude = StringField('Longitude', validators=[Optional()])
    latitude = StringField('Latitude', validators=[Optional()])


class ClientForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    url = StringField('Url', validators=[Optional()])
    address = StringField('Address', validators=[DataRequired()])
    city_id = SelectField('City', coerce=int, validators=[Optional()])
    state_id = SelectField('State', coerce=int, validators=[DataRequired()])
    country_id = SelectField('Country', coerce=int, validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired()])
    sub_domain = StringField('Sub Domain', validators=[Optional()])

class UpdateCustomerForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    address = StringField('Address', validators=[DataRequired()])
    phone = StringField('Phone', validators=[DataRequired()])
    city_id = SelectField('City', coerce=int, validators=[Optional()])
    state_id = SelectField('State', coerce=int, validators=[DataRequired()])
    country_id = SelectField('Country', coerce=int, validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired()])


class UserForm(AddressForm):
    client_id = IntegerField('Client ID', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])

    def validate_email(self, field):
        """
        validate user id
        :param field:
        :return:
        """

        if not field.data:
            raise ValidationError('This field is required.')

        user = models.User.query.filter(models.User.email == field.data).first()

        if user:
            raise ValidationError("User with this email - %s already exist" % field.data)


class GeoJsonForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    latitude = FloatField('Latitude', validators=[DataRequired()])
    longitude = FloatField('Longitude', validators=[DataRequired()])


class DeviceTypeForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])


class ReadingForm(FlaskForm):
    degree = FloatField('Degree', validators=[DataRequired()])
    humidity = FloatField('Humidity', validators=[Optional()])
    device_code = StringField('Device Reference', validators=[DataRequired()])
    voltage = FloatField('Volatge', validators=[Optional()])
    current = FloatField('Current', validators=[Optional()])
    power = FloatField('Power', validators=[Optional()])
    transformer_model_number = FloatField('Transformer Model Number', validators=[Optional()])


class DeviceForm(GeoJsonForm):
    reference_code = StringField('Reference ID', validators=[DataRequired()])
    meter_reference_code = StringField('Meter Reference ID', validators=[Optional()])
    client_id = IntegerField('Client', validators=[DataRequired()])
    is_master = BooleanField('Master', default=False)
    is_slave = BooleanField('Slave', default=False)
    customer_id = SelectField('Customer', coerce=int, validators=[Optional()])
    installation_id = SelectField('Installation', coerce=int, validators=[Optional()])


class NetworkForm(FlaskForm):
    # geojson_address_id = SelectField('GeoJsonAddress', coerce=int, validators=[DataRequired()])
    # location_id = IntegerField('Location', validators=[DataRequired()])
    client_id = IntegerField('Client', validators=[DataRequired()])
    station_id = IntegerField('Station', validators=[DataRequired()])
    product_id = IntegerField('Product', validators=[DataRequired()])


class InstallationForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    model_number = StringField('Model Number', validators=[DataRequired()])
    capacity = FloatField('Capacity', validators=[Optional()])
    measurement_index = StringField('Measurement Index', validators=[Optional()])
    client_id = IntegerField('Client', validators=[DataRequired()])
    installation_type_id = IntegerField('Installation Type', validators=[DataRequired()])
    service_id = IntegerField('Service', validators=[DataRequired()])
    location_id = IntegerField('Location', validators=[DataRequired()])
    station_id = IntegerField('Station', validators=[DataRequired()])
    product_id = IntegerField('Product', validators=[DataRequired()])


class SubStationForm(LocationForm):
    name = StringField('SubStation Name', validators=[DataRequired()])
    client_id = IntegerField('Client', validators=[DataRequired()])
    manager_id = IntegerField('Manager', validators=[DataRequired()])


class BillingRecordForm(FlaskForm):
    client_id = IntegerField('Client', validators=[DataRequired()])
    customer_id = SelectField('Customer', coerce=int, validators=[DataRequired()])
    billing_id = SelectField('Billing', coerce=int, validators=[DataRequired()])
    device_id = SelectField('Device', coerce=int, validators=[DataRequired()])


class BillingForm(FlaskForm):
    client_id = IntegerField('Client', validators=[DataRequired()])
    customer_id = SelectField('Customer', coerce=int, validators=[DataRequired()])
    start_date = DateField('Start Date', validators=[DataRequired()])
    end_date = DateField('Device', validators=[DataRequired()])
    amount = FloatField('Amount', validators=[DataRequired()])


class ClientBillingRecordForm(FlaskForm):
    client_id = IntegerField('Client', validators=[DataRequired()])
    customer_id = SelectField('Customer', coerce=int, validators=[DataRequired()])
    billing_id = SelectField('Billing', coerce=int, validators=[DataRequired()])
    device_id = SelectField('Device', coerce=int, validators=[DataRequired()])


class ClientBillingForm(FlaskForm):
    client_id = IntegerField('Client', validators=[DataRequired()])
    customer_id = SelectField('Customer', coerce=int, validators=[DataRequired()])
    billing_id = SelectField('Billing', coerce=int, validators=[DataRequired()])
    device_id = SelectField('Device', coerce=int, validators=[DataRequired()])


class CardForm(AddressForm):
    client_id = IntegerField('Client', validators=[Optional()])
    customer_id = SelectField('Customer', coerce=int, validators=[Optional()])
    # wallet_id = SelectField('Wallet', coerce=int, validators=[DataRequired()])
    card_number = StringField('Card Number', validators=[DataRequired()])
    card_expiry = StringField('Card Expiry', validators=[DataRequired()])
    brand = StringField('Card Brand', validators=[DataRequired()])
    cvv = StringField('CVV', validators=[DataRequired()])


class AddCardVerfForm(FlaskForm):
    card_data = StringField('Card', validators=[DataRequired()])
    bvn = StringField('BVN', validators=[DataRequired()])


class BankAccountForm(FlaskForm):
    client_id = IntegerField('Client', validators=[Optional()])
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    account_number = StringField('Account Number', validators=[DataRequired()])
    bank_id = SelectField('Bank', coerce=int, validators=[DataRequired()])


class BankForm(FlaskForm):
    name = StringField('Bank Name', validators=[DataRequired()])
    code = StringField('Bank Code', validators=[DataRequired()])


class InvoiceForm(FlaskForm):
    client_id = IntegerField('Client', validators=[DataRequired()])
    customer_id = SelectField('Customer', coerce=int, validators=[DataRequired()])
    billing_id = SelectField('Billing', coerce=int, validators=[DataRequired()])
    device_id = SelectField('Device', coerce=int, validators=[DataRequired()])


class SubscriptionForm(FlaskForm):
    client_id = IntegerField('Client', validators=[DataRequired()])
    service_id = SelectField('Billing', coerce=int, validators=[DataRequired()])

    def validate_client_id(self, field):
        """
        validate fields
        :return:
        """

        if not field.data:
            raise ValidationError('This field is required.')

        client = models.Client.query.get(field.data)

        if not client:
            raise ValidationError("Client with id - %s doesn't exist" % field.data)

    def validate_service_id(self, field):
        """
        validate fields
        :return:
        """

        if not field.data:
            raise ValidationError('This field is required.')

        service = models.Service.query.get(field.data)

        if not service:
            raise ValidationError("Service with id - %s doesn't exist" % field.data)


class DeleteObjectForm(FlaskForm):
    id = IntegerField('Object ID', validators=[DataRequired()])


class BillingThresholdForm(FlaskForm):
    debt_threshold_warning = IntegerField('Debt Threshold Warning Amount', validators=[DataRequired()], default=2000)
    allow_debt_warning = BooleanField('Allow Debt Warning', default=False)


class AlertTypeForm(FlaskForm):
    parameter = StringField('Parameter', validators=[DataRequired()])
    description = StringField('Description', validators=[Optional()])


class AlertForm(FlaskForm):
    lower_threshold = FloatField('Threshold', validators=[DataRequired()])
    upper_threshold = FloatField('Threshold', validators=[DataRequired()])
    description = StringField('Description', validators=[Optional()])
    client_id = IntegerField('Client', validators=[DataRequired()])
    alert_type_id = IntegerField('Alert Type', validators=[DataRequired()])
    installation_id = IntegerField('Installation', validators=[DataRequired()])
    device_id = IntegerField('Device', validators=[DataRequired()])
    service_id = IntegerField('Service', validators=[DataRequired()])
    station_id = IntegerField('Station', validators=[DataRequired()])


class InstallationTypeForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    code = StringField('Code', validators=[DataRequired()])


class ClientStaffForm(UserForm):
    access_group_ids = SelectMultipleField('Access Groups', coerce=int, widget=widgets.ListWidget(prefix_label=False),
                                           option_widget=widgets.CheckboxInput(), validators=[DataRequired()])

    def validate_email(form, field):
        if models.User.query.filter(models.User.email == field.data).count() != 0:
            raise ValidationError("This email address is already in use")

    def validate_password(form, field):
        if not check_password(field.data):
            raise ValidationError("Your password should be a minimum of 6 characters and contain at least one number, "
                                  "one uppercase, one lower case and one special character")


class ManageClientStaffForm(AddressForm):
    access_group_ids = SelectMultipleField('Access Groups', coerce=int, widget=widgets.ListWidget(prefix_label=False),
                                           option_widget=widgets.CheckboxInput(), validators=[DataRequired()])


class AccessGroupForm(FlaskForm):
    name = StringField('Access Group Name', validators=[DataRequired()])
    description = StringField('Description', validators=[Optional()])
    client_id = IntegerField('Client', validators=[Optional()])
    role_ids = SelectMultipleField('Access Group Permissions', coerce=int,
                                   widget=widgets.ListWidget(prefix_label=False), option_widget=widgets.CheckboxInput(),
                                   validators=[DataRequired()])
    is_update = BooleanField('Updating?', validators=[Optional()], default=False)

    def validate_name(form, field):
        if not form.is_update:
            client = models.Client.query.get(form.client_id.data)
            if client and models.AccessGroup.query.filter(models.AccessGroup.slug == slugify(field.data),
                                                          models.AccessGroup.client_id == client.id).first():
                raise ValidationError("Access Group matching same name already exist.")


class RoleForm(FlaskForm):
    name = StringField('Role Name', validators=[DataRequired()])
    title = StringField('Role Title', validators=[DataRequired()])


class ProductForm(FlaskForm):
    name = StringField('Product Name', validators=[DataRequired()])
    code = StringField('Product Code', validators=[DataRequired()])
    description = StringField('Product Description', validators=[DataRequired()])


class QueryForm(FlaskForm):
    start_date = FloatField(validators=[DataRequired()])
    end_date = FloatField(validators=[DataRequired()])


class NetworkRequestForm(FlaskForm):
    client_id = IntegerField('Client', validators=[DataRequired()])
    product_id = IntegerField('Product', validators=[DataRequired()])
    station_id = IntegerField('Station', validators=[DataRequired()])


class DeviceRequestForm(FlaskForm):
    product_id = IntegerField('Product', validators=[DataRequired()])
    installation_id = IntegerField('Installation', validators=[DataRequired()])


class CustomerRequestForm(FlaskForm):
    client_id = IntegerField('Client', validators=[DataRequired()])
    product_id = IntegerField('Product', validators=[DataRequired()])
    customer_id = IntegerField('Customer', validators=[DataRequired()])
    network_id = IntegerField('Network', validators=[DataRequired()])


class GeojsonPointForm(FlaskForm):
    longitude = FloatField('Longitude', validators=[DataRequired()])
    latitude = FloatField('Latitude', validators=[DataRequired()])


class FaultForm(FlaskForm):
    identifier = StringField('Identifier', validators=[DataRequired()])
    note = StringField('Note', validators=[Optional()])


class ReportForm(FlaskForm):
    start = DateTimeField("Start Date", format="%m/%d/%Y", validators=[DataRequired()], default=datetime.today())
    end = DateTimeField("End Date", format="%m/%d/%Y", default=datetime.today() + timedelta(days=1),
                        validators=[DataRequired()])
    data_types = RadioField('Data Type', coerce=str, validators=[DataRequired()],
                            description="Please Specify Data format")
    data_type = SelectField('Data Type', validators=[DataRequired()], coerce=str,
                            description="Please Specify Data format")

    def validate_end(form, field):
        if form.start.data > field.data:
            raise ValidationError("End date cannot be older than the start date")


class CustomerForm(AddressForm):
    client_id = IntegerField('Client', validators=[DataRequired()])
    product_id = IntegerField('Product', validators=[DataRequired()])
    station_id = IntegerField('Station', validators=[DataRequired()])
    network_id = IntegerField('Network', validators=[DataRequired()])

    def validate_email(self, field):
        """
        validate user id
        :param field:
        :return:
        """

        if not field.data:
            raise ValidationError('This field is required.')

        user = models.User.query.filter(models.User.email == field.data).first()

        if user:
            raise ValidationError("User with this email - %s already exist" % field.data)


class AddDeviceForm(FlaskForm):
    device_id = IntegerField('Device', validators=[DataRequired()])


class ComposeMessageForm(FlaskForm):
    message_type = StringField('Message Type', validators=[DataRequired()])
    recipient_type = SelectField('Recipient', coerce=str, validators=[DataRequired()])
    customer_id = SelectField('Customer', coerce=int, validators=[Optional()])
    client_id = SelectField('Client', coerce=int, validators=[Optional()])
    subject = StringField('Subject', validators=[DataRequired()])
    body = StringField('Body', validators=[DataRequired()], widget=widgets.TextArea())

    def validate_customer_id(self, field):
        if self.recipient_type.data == "customer" and field.data in [0, None, "", "0"]:
            raise ValidationError("Please Select a Customer as Recipient")

    def validate_client_id(self, field):
        if self.recipient_type.data == "client" and field.data in [0, None, "", "0"]:
            raise ValidationError("Please Select a Client as Recipient")


class MessageProcessingForm(FlaskForm):
    """ Schema class for handling messages to clients from customers """

    class Meta:
        csrf = False

    is_admin_message = BooleanField('Is an Admin Message', validators=[DataRequired()], default=False)
    client_id = IntegerField('Client', validators=[DataRequired()])
    customer_id = IntegerField('Customer', validators=[Optional()])
    name = StringField('Name', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired()])
    phone = StringField('Phone', validators=[Optional()])
    subject = StringField('Subject', validators=[DataRequired()])
    client_read = BooleanField('Client Read', validators=[DataRequired()])
    admin_read = BooleanField('Admin Read', validators=[DataRequired()])
    customer_read = BooleanField('Customer Read', validators=[DataRequired()])
    customer_replied = BooleanField('Customer Replied', validators=[DataRequired()])
    client_replied = BooleanField('Client Replied', validators=[DataRequired()])
    admin_replied = BooleanField('Admin Replied', validators=[DataRequired()])
    client_inbox = BooleanField('Client Inbox', validators=[DataRequired()])
    client_outbox = BooleanField('Client Outbox', validators=[DataRequired()])
    body = StringField('Body', validators=[DataRequired()])


class MessageResponse(FlaskForm):
    message_id = IntegerField('Message ID', validators=[DataRequired()])
    user_id = IntegerField('User', validators=[DataRequired()])
    body = StringField('Body', validators=[DataRequired()])
