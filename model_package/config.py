import os


class DevelopmentConfig(object):
    """
    Base class config
    """
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:postgres@localhost/atele'
    SECRET_KEY = '\x91c~\xc0-\xe3\'f\xe19PAtE\x93\xe8\x91`uu"\xd0\xb6\x01/\x0c\xed\\\xbd]HLe\x99k\xf8'
    AES_SECRET_KEY = '\x91c~\xc0-\xe3\'fA\xe19tEPE\x93\xe8\x91`uu"\xd0\xb6\x01/\x0c\xed\\\xbd]LHe\x99k002\xf8'
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SERVICES_URL = 'http://localhost:5050'
    APP_URL = 'http://localhost:5800'
    ASSETS_DEBUG = True
    EMAIL_DEV_ONLY = True
    DEV_EMAILS =['kunle@atele.org', 'kunle@officemotion.net','kunsam002@yahoo.com','styccs@gmail.com']
    # DEV_EMAILS = ['kunle@atele.org','kunle@officemotion.net','kunsam002@yahoo.com']
    ADMIN_PASSWORD = "atele"
    ADMIN_EMAIL = "admin@atele.org"
    ADMIN_FULL_NAME = "Atele Admin"

    DEMO_EMAIL = 'demo@atele.org'
    DEMO_PASSWORD = 'atele'
    DEMO_FULL_NAME = 'Nikola Tesla'

    SETUP_DIR = os.path.join(os.path.dirname(os.path.abspath(__name__)), 'setup')
    REPORTS_DIR = os.path.join(os.path.dirname(os.path.abspath(__name__)), "reports")
    UPLOADS_DIR = os.path.join(os.path.dirname(os.path.abspath(__name__)), "uploads")
    STATIC_DIR = os.path.join(os.path.dirname(os.path.abspath(__name__)), "static")

    # flask wtf
    WTF_CSRF_ENABLED = False

    # mqtt settings
    MQTT_HOST = "localhost"
    MQTT_PORT = 1883
    MQTT_KEEP_ALIVE = 60
    MQTT_BIND_ADDRESS = ""
    MQTT_TOPIC_BASE = 'devices'
    MQTT_TOPIC = 'devices/#'
    MQTT_PING = 'ping/#'
    MQTT_QOS = 1
    MQTT_USERNAME = 'admin'
    MQTT_PASSWORD = 'e@volution1ary'
    MQTT_CLIENT_ID = '#system'
    MQTT_MAX_INFLIGHT_MESSAGES = 65000
    MQTT_MAX_QUEUED_MESSAGES = 0

    # support
    SUPPORT_URL = 'https://support.atele.org'

    # Flutterwave Test API
    FLUTTER_WAVE_BASE_URL = "http://staging1flutterwave.co:8080/pwc/rest/"
    FLUTTERWAVE_KEY = "tk_17ovVx3ad6yKA4WTctJO"
    FLUTTERWAVE_MERCHANT_ID = "tk_astehwz1H0"

    # mongodb integration
    MONGODB_DATABASE = 'atele'

    # influxDB settings
    INFLUX_DB = {
        'host': 'localhost',
        'port': 8086,
        'username': 'root',
        'password': 'root',
        'database': 'atele'
    }

    ODOO_CONFIG = {
        'address': 'http://localhost:8069',
        'host': 'localhost',
        'port': 8069,
        'username': 'admin',
        'password': 'admin',
        'database': 'odooAtele'
    }

    ODOO_ENDPOINTS = {
        'authenticate': '/xmlrpc/2/common',
        'models': '/xmlrpc/2/object'
    }

    DATA_TRANSFER_FEE_PER_KB = 10
    DEFAULT_DATA_SIZE = 1
    DEFAULT_SERVICE_CHARGE = 10000

    AUTH_TIMEOUT = 600

    # Configuring sentry logging
    SENTRY_DSN = "https://94b2f3546c0a496aa5215223e13801c8:ac9083d14db64339997710181fb496af@sentry.io/231796"
    SENTRY_INCLUDE_PATHS = ['atele']
    SENTRY_USER_ATTRS = ['email', 'is_active', 'is_verified', 'client_id']

    # push messaging
    PUSH_PUBLIC_KEY = 'BMOMM4_NjXkYKjI0iAqgmYWdaj-cvBTYoiStVWWY2KMFS_ivSeZcvjjP2crj6Pat62Aycv9bDwQ_6X29-4RH-Rg'

    DEVICE_REQUESTS = {
        'SENDER': "hardware@atele.org",
        'RECIPIENT': 'ademola@atele.org'
    }


class ProductionConfig(DevelopmentConfig):
    MQTT_HOST = "apps.atele.org"
    MQTT_PORT = 1883
    SERVICES_URL = 'http://apps.atele.org'
    DEBUG = False