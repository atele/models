from _socket import gethostbyname, gethostname
from datetime import datetime
import hashlib
import json
from logging import Formatter, ERROR, handlers

from sqlalchemy import func
from sqlalchemy import inspect
from sqlalchemy.orm.state import InstanceState
from sqlalchemy.orm.collections import InstrumentedList
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy.ext.hybrid import hybrid_property
from geoalchemy2 import Geometry, Geography, shape
from geoalchemy2.elements import WKBElement
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from celery import Celery

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt

from flask_login import UserMixin
from utilities import slugify, DateJSONEncoder

import config

db = SQLAlchemy()


def make_celery(app):
    """ Enables celery to run within the flask application context """

    celery = Celery(app.import_name, backend=app.config.get("CELERY_RESULT_BACKEND"),
                    broker=app.config.get("CELERY_BROKER_URL"))
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return celery.Task.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


def create_app():
    current_app = Flask(__name__)
    current_app.config.from_object(config.DevelopmentConfig)
    db.init_app(current_app)
    current_app.db = db
    current_app.bcrypt = Bcrypt(current_app)
    current_app.celery = make_celery(current_app)

    file_handler = handlers.RotatingFileHandler("/var/log/api/models.log",
                                                maxBytes=500 * 1024)
    file_handler.setLevel(ERROR)
    file_handler.setFormatter(Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(pathname)s:%(lineno)d]'
    ))
    current_app.logger.addHandler(file_handler)

    return current_app


app = create_app()
create_app().app_context().push()
bcrypt = app.bcrypt


# @login_manager.user_loader
# def load_user(user_id):
#     return User.query.get(user_id)


def slugify_from_name(context):
    return slugify(context.current_parameters['name'])


def generate_token_code(context):
    return hashlib.md5("%s:%s" % (context.current_parameters["email"], str(datetime.now()))).hexdigest()


def to_dict(inst):
    data = dict()
    cls = inst.__class__
    relationships = inspect(cls).relationships

    keys = relationships.keys()
    keys.extend(inst.__dict__.keys())

    for key in keys:

        variable = getattr(inst, key)

        if isinstance(variable, WKBElement):
            data[key] = shape.to_shape(variable).to_wkt()

        elif isinstance(variable, InstanceState):
            continue

        elif variable is not None and type(variable) == datetime:
            data[key] = variable.strftime("%b %d %Y %H:%M:%S")

        elif isinstance(variable, InstrumentedList) or variable.__class__.__name__ == 'AppenderBaseQuery':
            data[key] = [{k: v for k, v in l.__dict__.iteritems() if k != '_sa_instance_state' and
                          not isinstance(v, db.Model) and not isinstance(v, InstrumentedList) and
                          not v.__class__.__name__ == 'AppenderBaseQuery' and not isinstance(v, WKBElement)} for l in
                         variable]

        elif isinstance(variable, db.Model):
            data[key] = {k: v for k, v in variable.__dict__.iteritems() if k != '_sa_instance_state' and
                         not isinstance(v, db.Model) and not isinstance(v, InstrumentedList) and
                         not v.__class__.__name__ == 'AppenderBaseQuery' and not isinstance(v, WKBElement)}

        else:
            data[key] = variable if variable is not None else str()

    return data


def to_json(inst):
    return json.dumps(to_dict(inst), cls=DateJSONEncoder)


class AppMixin(object):
    """ Mixin class for general attributes and functions """

    @declared_attr
    def date_created(cls):
        return db.Column(db.DateTime, default=datetime.utcnow, index=True)

    @declared_attr
    def last_updated(cls):
        return db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow, index=True)

    @property
    def as_json(self):
        return to_json(self)

    @property
    def as_dict(self):
        return to_dict(self)


class ClientMixin(AppMixin):
    """ Mixin class for Client related attributes and functions """

    @declared_attr
    def client_id(cls):
        return db.Column(db.Integer, db.ForeignKey('client.id'), nullable=False, index=True)


class PasswordResetToken(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    customer_id = db.Column(db.Integer, nullable=True)
    user_id = db.Column(db.Integer, nullable=True)
    email = db.Column(db.String(200), nullable=False)
    code = db.Column(db.String(200), unique=False, default=generate_token_code)
    is_expired = db.Column(db.Boolean, default=False)


class City(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    slug = db.Column(db.String(200), unique=True, default=slugify_from_name, onupdate=slugify_from_name, index=True)
    code = db.Column(db.String(200), index=True)
    state_id = db.Column(db.Integer, db.ForeignKey('state.id'), nullable=False, index=True)
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False, index=True)


class State(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    slug = db.Column(db.String(200), unique=True, default=slugify_from_name, onupdate=slugify_from_name, index=True)
    code = db.Column(db.String(200), index=True)
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False, index=True)
    cities = db.relationship('City', backref='state', lazy='dynamic')


class Country(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    slug = db.Column(db.String(200), unique=True, default=slugify_from_name, onupdate=slugify_from_name, index=True)
    code = db.Column(db.String(200), index=True)
    enabled = db.Column(db.Boolean, default=False, index=True)
    states = db.relationship('State', backref='country', lazy='dynamic')
    cities = db.relationship('City', backref='country', lazy='dynamic')
    banks = db.relationship('Bank', backref='country', lazy='dynamic')


class Currency(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), index=True)
    code = db.Column(db.String(200), index=True)
    enabled = db.Column(db.Boolean, default=False, index=True)
    symbol = db.Column(db.String(200), index=True)
    payment_code = db.Column(db.String(200), index=True)


class Timezone(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), index=True)
    code = db.Column(db.String(200), index=True)
    offset = db.Column(db.String(200))  # UTC time


class Address(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String, nullable=True)
    last_name = db.Column(db.String, nullable=False)
    line1 = db.Column(db.String, nullable=False)
    line2 = db.Column(db.String, nullable=True)
    phone = db.Column(db.String(50), nullable=True, index=True)
    email = db.Column(db.String(255), unique=True, index=True)

    city_id = db.Column(db.ForeignKey('city.id'), nullable=True, index=True)
    city = db.relationship('City')

    state_id = db.Column(db.Integer, db.ForeignKey('state.id'), nullable=False, index=True)
    state = db.relationship('State')

    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False, index=True)
    country = db.relationship('Country')

    @property
    def full_name(self):
        return '%s %s' % (self.first_name, self.last_name)


class AccessGroup(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False, index=True)
    slug = db.Column(db.String(200), default=slugify_from_name, onupdate=slugify_from_name, index=True)
    # title = db.Column(db.String(200), nullable=False)
    description = db.Column(db.String(200))
    permanent = db.Column(db.Boolean, default=False, index=True)
    is_client = db.Column(db.Boolean, default=False, index=True)
    client_id = db.Column(db.Integer, db.ForeignKey('client.id'), nullable=True, index=True)
    client = db.relationship('Client')
    roles = db.relationship('Role', secondary="role_access_groups",
                            backref=db.backref('access_groups', lazy='dynamic'))

    __table_args__ = (
        UniqueConstraint('name', 'client_id', name='_access_group_name'),
    )


role_access_groups = db.Table('role_access_groups',
                              db.Column('role_id', db.Integer,
                                        db.ForeignKey('role.id')),
                              db.Column('access_group_id', db.Integer,
                                        db.ForeignKey('access_group.id'))
                              )


class Role(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=True, nullable=False, index=True)
    # title = db.Column(db.String(200), nullable=False, index=True)
    slug = db.Column(db.String(200), unique=True, default=slugify_from_name, onupdate=slugify_from_name, index=True)
    description = db.Column(db.String(200))
    client_id = db.Column(db.Integer, db.ForeignKey('client.id'), nullable=True, index=True)
    client = db.relationship('Client')
    is_admin = db.Column(db.Boolean, default=True, index=True)
    assignable = db.Column(db.Boolean, default=True, index=True)


# User Roles for each role
user_roles = db.Table('user_roles',
                      db.Column('user_id', db.Integer,
                                db.ForeignKey('user.id')),
                      db.Column('role_id', db.Integer,
                                db.ForeignKey('role.id'))
                      )


class RestrictedDomain(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    domain = db.Column(db.String(200), unique=True, nullable=False, index=True)


user_access_groups = db.Table('user_access_groups',
                              db.Column('user_id', db.Integer,
                                        db.ForeignKey('user.id')),
                              db.Column('access_group_id', db.Integer,
                                        db.ForeignKey('access_group.id'))
                              )


def update_user_name(id, name):
    user = User.query.get(id)
    if user:
        user.name_ = name
        db.session.add(user)
        db.session.commit()
    return True


class User(AppMixin, UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    address_id = db.Column(db.Integer, db.ForeignKey('address.id'), nullable=False, index=True)
    address = db.relationship('Address', cascade="all, delete-orphan", lazy='joined', single_parent=True)
    email = db.Column(db.String(255), unique=True, index=True)
    password = db.Column(db.Text, unique=False)
    is_active = db.Column(db.Boolean, default=False, index=True)
    is_verified = db.Column(db.Boolean, default=False, index=True)
    is_demo_account = db.Column(db.Boolean, default=False, index=True)
    is_blocked = db.Column(db.Boolean, default=False, index=True)
    client_id = db.Column(db.Integer, db.ForeignKey('client.id'), nullable=True, index=True)
    client = db.relationship('Client')
    login_count = db.Column(db.Integer, default=0)
    last_login_at = db.Column(db.DateTime)
    current_login_at = db.Column(db.DateTime)
    last_login_ip = db.Column(db.String(200))
    current_login_ip = db.Column(db.String(200))
    name_ = db.Column(db.String(200))
    access_groups = db.relationship('AccessGroup', secondary="user_access_groups",
                                    backref=db.backref('users', lazy='noload'))

    def __repr__(self):
        return '<User %r>' % self.email

    def update_last_login(self):
        """
        update login information
        :return:
        """
        try:
            if self.current_login_at is None and self.last_login_at is None:
                self.current_login_at = self.last_login_at = datetime.now()
                self.current_login_ip = self.last_login_ip = gethostbyname(gethostname())

            if self.current_login_at != self.last_login_at:
                self.last_login_at = self.current_login_at
                self.last_login_ip = self.current_login_ip
                self.current_login_at = datetime.now()
                self.current_login_ip = gethostbyname(gethostname())

            if self.last_login_at == self.current_login_at:
                self.current_login_at = datetime.now()
                self.current_login_ip = gethostbyname(gethostname())

            self.login_count += 1
            db.session.add(self)
            db.session.commit()
        except Exception, e:
            print(e)

    @property
    def name(self):
        name = self.address.full_name
        update_user_name(self.id, name)
        return name

    @hybrid_property
    def roles(self):
        """ Fetch user roles as a list of roles """
        roles = []
        for access_group in self.access_groups:
            for role in access_group.roles:
                roles.append(role)

        return list(set(roles))

    def add_role(self, role):
        """
        Adds a the specified role to the user
        :param role: Role ID or Role name or Role object to be added to the user

        """
        try:

            role_obj = Role.query.filter_by(name=role.lower()).first()

            if isinstance(role, Role) and hasattr(role, "id"):
                role_obj = role
            elif type(role) is int:
                role_obj = Role.query.get(role)

            if not role_obj:
                raise Exception("Specified role could not be found")

            if self.has_role(role_obj) is False:
                self.roles.append(role_obj)
                db.session.add(self)
                db.session.commit()
                return self
        except:
            db.session.rollback()
            raise

    def has_role(self, role):
        """
        Returns true if a user identifies with a specific role

        :param role: A role name or `Role` instance
        """
        return role in self.roles

    def add_access_group(self, access_group):
        """
        Adds a the specified access group to the user
        :param group: AccessGroup ID or AccessGroup name or AccessGroup object to be added to the user

        """
        try:
            group_obj = AccessGroup.query.filter_by(slug=access_group.lower()).first()

            if isinstance(access_group, AccessGroup) and hasattr(access_group, "id"):
                group_obj = access_group
            elif type(access_group) is int:
                group_obj = AccessGroup.query.get(access_group)

            if not group_obj:
                raise Exception("Specified access_group could not be found")

            if self.has_access_group(group_obj) is False:
                self.access_groups.append(group_obj)
                db.session.add(self)
                db.session.commit()
                return self
        except Exception, e:
            db.session.rollback()
            raise e

    def remove_access_group(self, access_group):
        """
        Adds a the specified access group to the user
        :param group: AccessGroup ID or AccessGroup name or AccessGroup object to be added to the user

        """
        try:
            group_obj = AccessGroup.query.filter_by(name=access_group.lower()).first()

            if isinstance(access_group, AccessGroup) and hasattr(access_group, "id"):
                group_obj = access_group
            elif type(access_group) is int:
                group_obj = AccessGroup.query.get(access_group)

            if not group_obj:
                raise Exception("Specified access_group could not be found")

            if self.has_access_group(group_obj) is False:
                self.access_groups.remove(group_obj)
                db.session.add(self)
                db.session.commit()
                return self
        except:
            db.session.rollback()
            raise

    def has_access_group(self, access_group):
        """
        Returns true if a user identifies with a specific access_group

        :param access_group: A access_group name or `AccessGroup` instance
        """
        return access_group in self.access_groups

    def get_authorization_token(self, expiration=app.config['AUTH_TIMEOUT']):
        """
        returns authorization token
        :return:
        """
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        user = User.query.get(data['id'])
        return user

    def check_password(self, password):
        """
        Checks the given password against the saved password

        :param password: password to check
        :type password: string

        :returns True or False
        :rtype: bool

        """
        return bcrypt.check_password_hash(self.password, password)

    def set_password(self, password):
        """
        Sets a new password for the user

        :param password: the new password
        :type password: string
        """

        self.password = bcrypt.generate_password_hash(password)
        db.session.add(self)
        db.session.commit()


class Admin(AppMixin, UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), unique=True)
    password = db.Column(db.Text, unique=False)

    @property
    def email(self):
        """
        email account of admin user
        :return:
        """
        return '%s.atele.org' % self.username

    def get_auth_token(self):
        """ Returns the user's authentication token """
        return hashlib.md5("%s:%s:admin" % (self.username, self.password)).hexdigest()

    def check_password(self, password):
        """
        Checks the given password against the saved password

        :param password: password to check
        :type password: string

        :returns True or False
        :rtype: bool

        """
        return bcrypt.check_password_hash(self.password, password)

    def set_password(self, password):
        """
        Sets a new password for the user

        :param password: the new password
        :type password: string
        """

        self.password = bcrypt.generate_password_hash(password)
        db.session.add(self)
        db.session.commit()


client_customers = db.Table('client_customers',
                            db.Column('client_id', db.Integer, db.ForeignKey('client.id')),
                            db.Column('customer_id', db.Integer, db.ForeignKey('customer.id')))

station_customers = db.Table('station_customers',
                             db.Column('station_id', db.Integer, db.ForeignKey('station.id')),
                             db.Column('customer_id', db.Integer, db.ForeignKey('customer.id')))


class Customer(AppMixin, UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True, index=True)
    password = db.Column(db.Text, unique=False)
    name_ = db.Column(db.String(200))
    address_id = db.Column(db.Integer, db.ForeignKey('address.id'), nullable=False, index=True)
    address = db.relationship('Address', cascade="all, delete-orphan", lazy='joined', single_parent=True)
    # station_id = db.Column(db.ForeignKey('station.id'), nullable=False)
    first_name = db.Column(db.String, nullable=True)
    last_name = db.Column(db.String, nullable=True)
    # wallet_id = db.Column(db.Integer, db.ForeignKey('wallet.id'), nullable=False, index=True)
    # cards = db.relationship('Card', backref='customer', lazy='dynamic')
    devices = db.relationship('Device', backref='customer', lazy='dynamic')
    login_count = db.Column(db.Integer, default=0)
    last_login_at = db.Column(db.DateTime)
    current_login_at = db.Column(db.DateTime)
    last_login_ip = db.Column(db.String(200))
    current_login_ip = db.Column(db.String(200))
    is_blocked = db.Column(db.Boolean, default=False, index=True)

    @property
    def name(self):
        name = self.address.full_name
        self.name_ = name
        try:
            db.session.add(self)
            db.session.commit()
        except:
            pass
        return name

    def update_last_login(self):
        """
        update login information
        :return:
        """
        try:
            if self.current_login_at is None and self.last_login_at is None:
                self.current_login_at = self.last_login_at = datetime.now()
                self.current_login_ip = self.last_login_ip = gethostbyname(gethostname())

            if self.current_login_at != self.last_login_at:
                self.last_login_at = self.current_login_at
                self.last_login_ip = self.current_login_ip
                self.current_login_at = datetime.now()
                self.current_login_ip = gethostbyname(gethostname())

            if self.last_login_at == self.current_login_at:
                self.current_login_at = datetime.now()
                self.current_login_ip = gethostbyname(gethostname())

            self.login_count += 1
            db.session.add(self)
            db.session.commit()
        except Exception, e:
            print(e)

    def check_password(self, password):
        """
        Checks the given password against the saved password

        :param password: password to check
        :type password: string

        :returns True or False
        :rtype: bool

        """
        return bcrypt.check_password_hash(self.password, password)

    def set_password(self, password):
        """
        Sets a new password for the user

        :param password: the new password
        :type password: string
        """

        self.password = bcrypt.generate_password_hash(password)
        db.session.add(self)
        db.session.commit()

    def get_authorization_token(self, expiration=app.config['AUTH_TIMEOUT']):
        """
        returns authorization token
        :return:
        """
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        user = User.query.get(data['id'])
        return user


class Client(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # address_id = db.Column(db.Integer, db.ForeignKey('address.id'), nullable=False, index=True)
    # address = db.relationship('Address', cascade="all, delete-orphan", lazy='joined', single_parent=True)
    name = db.Column(db.String(255), nullable=False)
    address = db.Column(db.Text, nullable=False)
    city_id = db.Column(db.ForeignKey('city.id'), nullable=True, index=True)
    city = db.relationship('City')

    state_id = db.Column(db.Integer, db.ForeignKey('state.id'), nullable=False, index=True)
    state = db.relationship('State')

    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False, index=True)
    country = db.relationship('Country')

    phone = db.Column(db.String(50), nullable=True, index=True)
    email = db.Column(db.String(255), unique=True, index=True)
    url = db.Column(db.String, nullable=True, index=True)
    sub_domain = db.Column(db.String(255), nullable=False)

    wallet = db.relationship('Wallet')

    devices = db.relationship('Device', backref='client', lazy='dynamic')
    installations = db.relationship('Installation', backref="installation", lazy="dynamic")
    # customers = db.relationship('Customer', backref='client', lazy='dynamic')
    settings = db.relationship('Settings', backref='client', uselist=False)
    cards = db.relationship('Card', backref='client', lazy='dynamic')
    bank_accounts = db.relationship('BankAccount', backref='client', lazy='dynamic')
    subscriptions = db.relationship('Subscription', backref='client', lazy='dynamic')
    reports = db.relationship('Report', backref='client', lazy='dynamic')
    invoices = db.relationship('Invoice', backref='client', lazy='dynamic')
    customers = db.relationship('Customer', secondary="client_customers", backref=db.backref('clients', lazy='dynamic'))

    @property
    def subscription_fee(self):
        return db.session.query(func.sum(Subscription.subscription_fee)).filter(
            Subscription.client_id == self.id).scalar()


# @property
# def name(self):
#     return self.address.full_name
#
# @property
# def sub_domain(self):
#     return '%s.atele.org' % slugify(self.name, '-')


class GeojsonAddress(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    polygon = db.Column(Geometry('POLYGON'), nullable=True)


class GeojsonPoint(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    point = db.Column(Geography(geometry_type='POINT', srid=4326))
    longitude = db.Column(db.String, nullable=False, index=True)
    latitude = db.Column(db.String, nullable=False, index=True)


class Location(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    line1 = db.Column(db.String, nullable=False)
    line2 = db.Column(db.String, nullable=True)

    city_id = db.Column(db.ForeignKey('city.id'), nullable=True, index=True)
    city = db.relationship('City')

    state_id = db.Column(db.Integer, db.ForeignKey('state.id'), nullable=False, index=True)
    state = db.relationship('State')

    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False, index=True)
    country = db.relationship('Country')

    longitude = db.Column(db.String, nullable=True, index=True)
    latitude = db.Column(db.String, nullable=True, index=True)

    @property
    def address(self):
        return '{} {} {}, {}, {}'.format(self.line1, self.line2, self.city.name, self.state.name, self.country.name)


class InstallationType(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, nullable=False)
    code = db.Column(db.String, nullable=False)
    html = db.Column(db.Text, nullable=False)
    script = db.Column(db.Text, nullable=False)
    services = db.relationship('Service', secondary='associated_services', backref=db.backref('installation_types',
                                                                                              lazy='dynamic'))


associated_services = db.Table('associated_services',
                               db.Column('service_id', db.Integer, db.ForeignKey('service.id')),
                               db.Column('installation_type_id', db.Integer, db.ForeignKey('installation_type.id'))
                               )


class Installation(ClientMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, nullable=False)
    code = db.Column(db.String, nullable=False)
    model_number = db.Column(db.String, nullable=False, index=True)
    capacity = db.Column(db.Float, nullable=True)
    measurement_index = db.Column(db.String, nullable=True)
    location_id = db.Column(db.Integer, db.ForeignKey('location.id'), nullable=False, index=True)
    client = db.relationship(Client)
    location = db.relationship(Location)
    station_id = db.Column(db.Integer, db.ForeignKey('station.id'), nullable=False, index=True)
    service_id = db.Column(db.Integer, db.ForeignKey('service.id'), index=True, nullable=False)
    service = db.relationship('Service')
    installation_type_id = db.Column(db.Integer, db.ForeignKey('installation_type.id'), index=True, nullable=False)
    installation_type = db.relationship(InstallationType)
    devices = db.relationship('Device')

    __table_args__ = (
        UniqueConstraint('model_number', 'client_id', name='_installation_model_number'),
    )


class Network(ClientMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String, nullable=False)
    client = db.relationship(Client)
    devices = db.relationship('Device', backref='network', lazy='dynamic')
    geojson_address_id = db.Column(db.Integer, db.ForeignKey('geojson_address.id'), nullable=True, index=True)
    station_id = db.Column(db.Integer, db.ForeignKey('station.id'), nullable=True, index=True)
    station = db.relationship('Station', uselist=False)
    services = db.relationship('Service', secondary="network_services", backref=db.backref('networks', lazy='dynamic'))


station_assignees = db.Table('station_assignees',
                             db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
                             db.Column('station_id', db.Integer, db.ForeignKey('station.id'))
                             )

network_services = db.Table('network_services',
                            db.Column('network_id', db.Integer, db.ForeignKey('network.id')),
                            db.Column('service_id', db.Integer, db.ForeignKey('service.id')))


class Device(ClientMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String, nullable=False, index=True)
    reference_code = db.Column(db.String, nullable=True, index=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=True, index=True)
    point_id = db.Column(db.Integer, db.ForeignKey('geojson_point.id'), nullable=False, index=True)
    point = db.relationship('GeojsonPoint')

    is_coordinator = db.Column(db.Boolean, default=False, index=True)
    is_node = db.Column(db.Boolean, default=True, index=True)
    is_active = db.Column(db.Boolean, default=True, index=True)

    product_id = db.Column(db.Integer, db.ForeignKey('product.id'), nullable=False, index=True)
    product = db.relationship('Product')

    network_id = db.Column(db.Integer, db.ForeignKey('network.id'), nullable=True, index=True)
    station_id = db.Column(db.Integer, db.ForeignKey('station.id'), nullable=True, index=True)
    station = db.relationship('Station')

    installation_id = db.Column(db.Integer, db.ForeignKey('installation.id'), nullable=True, index=True)
    installation = db.relationship('Installation')


class DeviceType(ClientMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False, index=True)
    slug = db.Column(db.String(200), unique=True, default=slugify_from_name, onupdate=slugify_from_name, index=True)
    # devices = db.relationship('Device', backref='device_type', lazy='dynamic')


class CustomerBillingRecord(ClientMixin, db.Model):
    """
    BillingRecord model object are records of use for an individual device
    """
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.Text, nullable=False)
    customer_billing_id = db.Column(db.Integer, db.ForeignKey('customer_billing.id'), nullable=False, index=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=False, index=True)
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'), nullable=False, index=True)
    network_id = db.Column(db.Integer, db.ForeignKey('network.id'), nullable=True, index=True)


class CustomerBilling(ClientMixin, db.Model):
    """
        Billing model object details all billing records of a client over a period
    """
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.Text, nullable=False)
    client = db.relationship(Client)
    start_date = db.Column(db.DateTime, nullable=False)
    end_date = db.Column(db.DateTime, nullable=False)
    amount = db.Column(db.Float, nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=False, index=True)
    records = db.relationship(CustomerBillingRecord)


class BillingRecord(ClientMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    billing_id = db.Column(db.Integer, db.ForeignKey('billing.id'), nullable=False, index=True)
    subscription_id = db.Column(db.Integer, db.ForeignKey('subscription.id'), nullable=False, index=True)
    installation_id = db.Column(db.Integer, db.ForeignKey('installation.id'), nullable=True, index=True)
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'), nullable=False, index=True)
    network_id = db.Column(db.Integer, db.ForeignKey('network.id'), nullable=True, index=True)
    data_size = db.Column(db.Float, default=app.config['DEFAULT_DATA_SIZE'])
    amount = db.Column(db.Float,
                       default=app.config['DEFAULT_DATA_SIZE'] * app.config['DEFAULT_SERVICE_CHARGE'])


class Billing(ClientMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Float, nullable=True)
    records = db.relationship(BillingRecord)
    invoice_id = db.Column(db.Integer, db.ForeignKey('invoice.id'), nullable=False, index=True)


class Deduction(ClientMixin, db.Model):
    """
    Client billing deductions
    """
    id = db.Column(db.Integer, primary_key=True)
    billing_id = db.Column(db.Integer, db.ForeignKey('billing.id'), nullable=False)
    billing_record_id = db.Column(db.Integer, db.ForeignKey('billing_record.id'), nullable=False)


class CustomerDeduction(ClientMixin, db.Model):
    """
    Customer billing deductions
    """
    id = db.Column(db.Integer, primary_key=True)
    customer_billing_id = db.Column(db.Integer, db.ForeignKey('customer_billing.id'), nullable=False)
    customer_billing_record_id = db.Column(db.Integer, db.ForeignKey('customer_billing_record.id'), nullable=False)


class Invoice(ClientMixin, db.Model):
    """
    Client billing invoice
    """
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.Text, nullable=False)
    # client = db.relationship(Client)
    billings = db.relationship(Billing)
    month = db.Column(db.Text, nullable=False)
    year = db.Column(db.Text, nullable=False)
    is_paid = db.Column(db.Boolean, default=False)
    service_charge = db.Column(db.Float, default=0.0)
    billing_amount = db.Column(db.Float, default=0.0)
    total_charge = db.Column(db.Float, default=0.0)

    @property
    def _billing_amount(self):
        amount = db.session.query(func.sum(Billing.amount)).filter(
            Billing.client_id == self.client_id, Billing.invoice_id == self.id).scalar()
        self.billing_amount = amount
        db.session.add(self)
        db.session.commit()
        return amount

    @property
    def _total_charge(self):
        billing = self.billing_amount if self.billing_amount else 0
        charge = billing + self.service_charge
        self.total_charge = charge
        db.session.add(self)
        db.session.commit()
        return charge

    def calculate_charge(self):
        self._billing_amount()
        self._total_charge()
        return True


class CustomerInvoice(ClientMixin, db.Model):
    """
    Customer billing invoice
    """
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.Text, nullable=False)
    client = db.relationship(Client)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=False, index=True)
    customer = db.relationship(Customer)
    customer_billing_id = db.Column(db.Integer, db.ForeignKey('customer_billing.id'), index=True, nullable=False)
    customer_billing = db.relationship(CustomerBilling)


class Receipt(ClientMixin, db.Model):
    """
    Client receipt for invoice
    """
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.Text, nullable=False)
    client = db.relationship(Client)
    invoice_id = db.Column(db.Integer, db.ForeignKey('invoice.id'), nullable=False, index=True)
    invoice = db.relationship(Invoice)


class CustomerReceipt(ClientMixin, db.Model):
    """
    Customer receipt for invoice
    """
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.Text, nullable=False)
    client = db.relationship(Client)
    customer_invoice_id = db.Column(db.Integer, db.ForeignKey('customer_invoice.id'), nullable=False, index=True)
    customer_invoice = db.relationship(CustomerInvoice)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=False, index=True)
    customer = db.relationship(Customer)


class Wallet(ClientMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    debt_balance = db.Column(db.Float, default=0.0)
    credit_balance = db.Column(db.Float, default=0.0)
    # customers = db.relationship('Customer', backref="wallet", lazy="dynamic")
    cards = db.relationship('Card', backref="wallet", lazy="dynamic")


class Card(ClientMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=True, index=True)
    wallet_id = db.Column(db.Integer, db.ForeignKey('wallet.id'), nullable=True, index=True)
    address_id = db.Column(db.Integer, db.ForeignKey('address.id'), nullable=False, index=True)
    address = db.relationship('Address')
    card_number = db.Column(db.Text)
    token = db.Column(db.String(200), nullable=False)
    mask = db.Column(db.String(300), nullable=True)
    brand = db.Column(db.String(300), nullable=True)
    exp_month = db.Column(db.Integer, nullable=True)
    exp_year = db.Column(db.Integer, nullable=True)


class BankAccount(ClientMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(200), nullable=False)
    last_name = db.Column(db.String(200), nullable=False)
    account_number = db.Column(db.String(200), nullable=False, index=True)
    bank_id = db.Column(db.Integer, db.ForeignKey('bank.id'), nullable=False, index=True)


class Bank(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    code = db.Column(db.String(200), nullable=False)
    country_code = db.Column(db.String(200), nullable=False)
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False, index=True)
    branch = db.Column(db.String(200), nullable=False)
    accounts = db.relationship('BankAccount', backref='bank', lazy='dynamic')


class Subscription(ClientMixin, db.Model):
    """
    Client subscription to services
    """
    id = db.Column(db.Integer, primary_key=True)
    service_id = db.Column(db.Integer, db.ForeignKey('service.id'), nullable=False, index=True)
    service = db.relationship('Service')
    subscription_fee = db.Column(db.Float, default=app.config['DEFAULT_SERVICE_CHARGE'])
    is_active = db.Column(db.Boolean, default=False)
    devices_dep_resolved = db.Column(db.Boolean, default=False)
    alerts_dep_resolved = db.Column(db.Boolean, default=False)
    inst_dep_resolved = db.Column(db.Boolean, default=False)


class SubNavigation(AppMixin, db.Model):
    """
    Sub navigation models for service navigations
    """
    id = db.Column(db.Integer, primary_key=True)
    navigation_id = db.Column(db.Integer, db.ForeignKey('navigation.id'), nullable=False, index=True)
    name = db.Column(db.String(255), nullable=False)
    code = db.Column(db.String(50), nullable=False)
    url = db.Column(db.Text, nullable=False)
    is_img = db.Column(db.Boolean, default=False)
    icon = db.Column(db.Text, nullable=False)


class Navigation(AppMixin, db.Model):
    """
    Navigation model for services
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    code = db.Column(db.String(50), nullable=False)
    url = db.Column(db.Text, nullable=False)
    icon = db.Column(db.Text, nullable=False)
    is_img = db.Column(db.Boolean, default=False)
    # service_id = db.Column(db.Integer, db.ForeignKey('service.id'))
    sub_navs = db.relationship(SubNavigation)


class Service(AppMixin, db.Model):
    """
    Service model describing available services
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    code = db.Column(db.String(255), nullable=False, index=True, unique=True)
    icon_class = db.Column(db.String, nullable=True)
    is_active = db.Column(db.Boolean, default=False)
    parent_id = db.Column(db.Integer, nullable=True)
    description = db.Column(db.Text)
    subscriptions = db.relationship(Subscription)
    # navigations = db.relationship(Navigation)
    products = db.relationship('Product', secondary="product_services", backref=db.backref('services', lazy='dynamic'))
    service_charge = db.Column(db.Float, default=app.config['DEFAULT_SERVICE_CHARGE'])


station_services = db.Table('station_services',
                            db.Column('service_id', db.Integer, db.ForeignKey('service.id')),
                            db.Column('station_id', db.Integer, db.ForeignKey('station.id'))
                            )


class Station(ClientMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    code = db.Column(db.String, nullable=True, index=True)
    is_active = db.Column(db.Boolean, default=True)
    location_id = db.Column(db.Integer, db.ForeignKey('location.id'), nullable=False, index=True)
    location = db.relationship('Location')

    manager_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, index=True)
    manager = db.relationship('User')
    assignees = db.relationship(User, secondary=station_assignees, backref=db.backref('station_assignees',
                                                                                      lazy='dynamic'))
    station_services = db.relationship(Service, secondary=station_services, backref=db.backref('station_services',
                                                                                               lazy='dynamic'))
    installations = db.relationship(Installation)
    devices = db.relationship(Device)
    networks = db.relationship(Network)
    client = db.relationship('Client')
    customers = db.relationship('Customer', secondary="station_customers",
                                backref=db.backref('stations', lazy='dynamic'))

    @property
    def city(self):
        return self.location.city

    @property
    def state(self):
        return self.location.state

    @property
    def country(self):
        return self.location.country

    @property
    def nodes(self):
        return self.devices.filter_by(is_node=True)


class Settings(ClientMixin, db.Model):
    """
    Configurable settings controlling client billing periods
    """
    id = db.Column(db.Integer, primary_key=True)
    allow_debt_warning = db.Column(db.Boolean, default=False, index=True)
    debt_threshold_warning = db.Column(db.Integer, default=2000)
    billing_period = db.Column(db.Integer, default=1, index=True)  # in months
    is_postpaid = db.Column(db.Boolean, default=True, index=True)
    logo = db.Column(db.Text, nullable=True)

    alerts_dep_resolved = db.Column(db.Boolean, default=False)
    stations_dep_resolved = db.Column(db.Boolean, default=False)
    accounts_dep_resolved = db.Column(db.Boolean, default=False)
    bank_account_dep_resolved = db.Column(db.Boolean, default=False)
    billing_dep_resolved = db.Column(db.Boolean, default=False)


class CustomerSettings(AppMixin, db.Model):
    """
    Configurable settings controlling client billing periods
    """
    id = db.Column(db.Integer, primary_key=True)
    customer_id = db.Column(db.ForeignKey('customer.id'), nullable=False)
    debt_threshold_warning = db.Column(db.Float, default=2000)
    sms_notification = db.Column(db.Boolean, default=False, index=True)  # in months
    email_notification = db.Column(db.Boolean, default=True, index=True)


class TableDesc(AppMixin, db.Model):
    """
    schema descriptions for each table
    """
    id = db.Column(db.Integer, primary_key=True)
    cls = db.Column(db.String, nullable=False)
    description = db.Column(db.Text, nullable=False)

    def __repr__(self):
        return '%s: %s' % (self.cls, self.description)


class RecurrentCard(ClientMixin, db.Model):
    """
    Recurrent cards
    """
    id = db.Column(db.Integer, primary_key=True)
    token = db.Column(db.String(200), nullable=False)
    mask = db.Column(db.String(300), nullable=True)
    brand = db.Column(db.String(300), nullable=True)
    exp_month = db.Column(db.Integer, nullable=True)
    exp_year = db.Column(db.Integer, nullable=True)


class AlertChannel(AppMixin, db.Model):
    """
    alert type describing threshold and measurement parameters
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    slug = db.Column(db.String, default=slugify_from_name, onupdate=slugify_from_name, nullable=False)


class AlertType(AppMixin, db.Model):
    """
    alert type describing threshold and measurement parameters
    """
    id = db.Column(db.Integer, primary_key=True)
    parameter = db.Column(db.String, nullable=False)
    metric = db.Column(db.String, nullable=False)
    symbol = db.Column(db.String, nullable=False)
    description = db.Column(db.Text, nullable=True)


associated_channels = db.Table('associated_channels',
                               db.Column('alert_channel_id', db.Integer, db.ForeignKey('alert_channel.id')),
                               db.Column('alert_id', db.Integer, db.ForeignKey('alert.id'))
                               )


class Alert(ClientMixin, db.Model):
    """
    system alerts to notify operators when downtime happens
    """
    id = db.Column(db.Integer, primary_key=True)
    threshold = db.Column(db.Float, nullable=False)
    code = db.Column(db.Text, nullable=False)
    is_critical = db.Column(db.Boolean, default=True)
    is_warning = db.Column(db.Boolean, default=False)
    alert_type_id = db.Column(db.Integer, db.ForeignKey('alert_type.id'))
    alert_type = db.relationship(AlertType)
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'), nullable=True)
    device = db.relationship(Device)
    installation_id = db.Column(db.Integer, db.ForeignKey('installation.id'), nullable=True)
    installation = db.relationship(Installation)
    service_id = db.Column(db.Integer, db.ForeignKey('service.id'), nullable=False)
    service = db.relationship(Service)
    station_id = db.Column(db.Integer, db.ForeignKey('station.id'), nullable=False)
    station = db.relationship(Station)
    channels = db.relationship(AlertChannel, secondary='associated_channels', backref=db.backref('alert',
                                                                                                 lazy='dynamic'))
    recipients = db.relationship('AlertRecipient', backref=db.backref('alert'))


class AlertRecipient(ClientMixin, db.Model):
    """
    alert recipients`
    """
    id = db.Column(db.Integer, primary_key=True)
    alert_id = db.Column(db.Integer, db.ForeignKey('alert.id'), nullable=False)
    recipient = db.Column(db.Text, nullable=False)
    is_device = db.Column(db.Boolean, default=False)
    is_email = db.Column(db.Boolean, default=True)
    is_push = db.Column(db.Boolean, default=True)


class Metric(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    slug = db.Column(db.String(200), unique=True, default=slugify_from_name, onupdate=slugify_from_name, index=True)
    dimension = db.Column(db.String(20), nullable=False)


class Product(AppMixin, db.Model):
    """
    product model
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text, nullable=False)
    code = db.Column(db.Text, nullable=False)
    # barcode = db.Column(db.Text, nullable=False)
    on_default = db.Column(db.Boolean, default=False)
    is_network = db.Column(db.Boolean, default=False, index=True)
    is_alert = db.Column(db.Boolean, default=False, index=True)

    metrics = db.relationship('Metric', secondary="product_metrics", backref=db.backref('products', lazy='dynamic'))


product_metrics = db.Table('product_metrics',
                           db.Column('metric_id', db.Integer, db.ForeignKey('metric.id')),
                           db.Column('product_id', db.Integer, db.ForeignKey('product.id')))


class ProductImage(AppMixin, db.Model):
    """
    product image
    """
    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'), nullable=False)
    image_url = db.Column(db.Text, nullable=False)


product_services = db.Table('product_services',
                            db.Column('product_id', db.Integer, db.ForeignKey('product.id')),
                            db.Column('service_id', db.Integer, db.ForeignKey('service.id'))
                            )


class Report(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    handle = db.Column(db.String(200), nullable=False)
    name = db.Column(db.String(200), nullable=False)
    description = db.Column(db.Text)
    start = db.Column(db.DateTime)
    end = db.Column(db.DateTime)
    client_id = db.Column(db.Integer, db.ForeignKey('client.id'), nullable=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=True)
    file_path = db.Column(db.Text, nullable=True)
