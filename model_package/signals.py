"""
signals.py

All signals required within the application.
These signals will be used to interconnect different parts of the application.
It will also be used by 3rd party applications as a connection mechanism
"""

from blinker import Namespace

app_signals = Namespace()

# Send this signal when a report has been created
report_generated = app_signals.signal('report_generated')

# Send this signal when a card record has been added
card_added = app_signals.signal('card_added')

# Send this signal at the end of a month notifying client of debt to pay
invoice_payment_notification = app_signals.signal('invoice_payment_notification')

# Send this signal at the end of a month notifying client of debt to pay
user_password_reset_requested = app_signals.signal('user_password_reset_requested')

# Send this signal at the end of a month notifying client of debt to pay
customer_password_reset_requested = app_signals.signal('customer_password_reset_requested')

# customer account created
customer_account_created = app_signals.signal('customer_account_created')


